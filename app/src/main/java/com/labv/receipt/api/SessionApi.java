package com.labv.receipt.api;

import retrofit.http.DELETE;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;
import retrofit.http.PUT;

public interface SessionApi {
    public static class Session {
        String idS;
        String userIdS;
        String sessionToken;

        public Session(String idS, String userIdS, String sessionToken) {
            this.idS = idS;
            this.userIdS = userIdS;
            this.sessionToken = sessionToken;
        }

        public String getIdS() {
            return idS;
        }

        public String getUserIdS() {
            return userIdS;
        }

        public String getSessionToken() {
            return sessionToken;
        }
    }

    @DELETE("/sessions")
    Session destroy();

    @FormUrlEncoded
    @POST("/sessions")
    Session create(
            @Field("email") String email,
            @Field("password") String password,
            @Field("device_id") String deviceId,
            @Field("platform") String platform
    );

    @FormUrlEncoded
    @PUT("/sessions")
    Session update(@Field("push_token") String pushToken);

    @FormUrlEncoded
    @POST("/sessions/reset_password")
    Session resetPassword(@Field("email") String email);
}
