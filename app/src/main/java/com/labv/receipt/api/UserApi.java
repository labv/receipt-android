package com.labv.receipt.api;

import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;

public interface UserApi {
    public static class User {
        String firstName;
        String lastName;
        String idS;
        String email;
        String dob;
        String password;
        boolean acceptTerms;
        String latestReceiptId;
        SessionApi.Session session;

        public User(String firstName, String lastName, String email, String dob, boolean acceptTerms, String password) {
            this.firstName = firstName;
            this.lastName = lastName;
            this.email = email;
            this.dob = dob;
            this.acceptTerms = acceptTerms;
            this.password = password;
        }

        public String getFirstName() {
            return firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public String getIdS() {
            return idS;
        }

        public String getEmail() {
            return email;
        }

        public String getDob() {
            return dob;
        }

        public String getPassword() {
            return password;
        }

        public boolean isAcceptTerms() {
            return acceptTerms;
        }

        public SessionApi.Session getSession() {
            return session;
        }

        public String getLatestReceiptId() {
            return latestReceiptId;
        }
    }

    @GET("/users/{id}")
    User get(@Path("id") String userId);

    @FormUrlEncoded
    @POST("/users")
    User create(
            @Field("first_name") String firstName,
            @Field("last_name") String lastName,
            @Field("password") String password,
            @Field("platform") String platform,
            @Field("email") String email,
            @Field("accept_terms") boolean acceptTerms
    );

    @FormUrlEncoded
    @PUT("/users/{id}")
    User update(
            @Path("id") String userId,
            @Field("first_name") String firstName,
            @Field("last_name") String lastName,
            @Field("email") String email
    );

    @FormUrlEncoded
    @PUT("/users/{id}")
    User updatePassword(
            @Path("id") String userId,
            @Field("old_password") String OldPassword,
            @Field("password") String password
    );
}
