package com.labv.receipt.api;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.internal.bind.DateTypeAdapter;
import com.labv.receipt.models.Session;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

public class ApiFactory<T> {
    private static class ApiConnection {
        String mHost;
        int mPort;

        public ApiConnection(int port, String host) {
            mPort = port;
            mHost = host;
        }
    }

    private static final Map<String, ApiConnection> CONNECTION_MAP = new HashMap<String, ApiConnection>() {{
        put("dev", new ApiFactory.ApiConnection(80, "http://api.dev.gettreebox.com"));
    }};

    private static ApiConnection apiConnection = null;

    public static void initialize(String environment) {
        apiConnection = CONNECTION_MAP.get(environment);
        if (apiConnection == null) {
            throw new IllegalArgumentException("Invalid environment: " + environment);
        }
    }

    protected ApiConnection mInfo;
    private final Class<T> type;

    protected ApiFactory(Class<T> type, ApiConnection connection) {
        this.type = type;
        this.mInfo = connection;
    }

    public String buildUrl(String url) {
        return apiConnection.mHost + ":" + apiConnection.mPort + "/" + url;
    }

    public T buildService(final Session session) {
        RequestInterceptor requestInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                request.addHeader("Authorization",
                    buildSessionHeader(session.getUserId(), session.getSessionId(), session.getSessionToken())
                );
            }
        };
        return _buildService(requestInterceptor);
    }

    public T buildService() {
       return _buildService(null);
    }

    private String buildSessionHeader(String userId, String sessionId, String sessionToken) {
        return String.format("ApiToken uid=%s, sid=%s, token=%s", userId, sessionId, sessionToken);
    }

    private T _buildService(RequestInterceptor interceptor) {
        Gson gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .setDateFormat("yyyy-MM-dd'T'HH:mm:sszzzz")
                .registerTypeAdapter(Date.class, new DateTypeAdapter())
                .create();

        RestAdapter.Builder builder = new RestAdapter.Builder()
                .setEndpoint(mInfo.mHost + ":" + mInfo.mPort)
                .setConverter(new GsonConverter(gson));

        if (interceptor != null) {
            builder = builder.setRequestInterceptor(interceptor);
        }

        RestAdapter restAdapter = builder.build();
        return restAdapter.create(type);
    }

    public static <V> ApiFactory<V> getInstance(Class<V> type) {
        return new ApiFactory<V>(type, apiConnection);
    }
}
