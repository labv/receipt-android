package com.labv.receipt.api;

import retrofit.http.GET;
import retrofit.http.Path;

public interface RetailerApi {
    public static class Retailer {
        String idS;
        Long createdAtI;
        Long updatedAtI;
        String name;
        String category;
        String logoImageUrl;

        public String getIdS() {
            return idS;
        }

        public Long getCreatedAtI() {
            return createdAtI;
        }

        public Long getUpdatedAtI() {
            return updatedAtI;
        }

        public String getName() {
            return name;
        }

        public String getCategory() {
            return category;
        }

        public String getLogoImageUrl() {
            return logoImageUrl;
        }
    }

    @GET("/retailers/{id}")
    Retailer get(@Path("id") String retailerId);
}
