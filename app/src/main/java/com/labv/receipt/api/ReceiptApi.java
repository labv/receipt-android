package com.labv.receipt.api;

import java.util.List;

import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.PUT;
import retrofit.http.Path;
import retrofit.http.Query;

public interface ReceiptApi {
    public static class Receipt {
        String idS;
        Long createdAtI;
        Long updatedAtI;
        Long deletedAtI;
        Long readAtI;
        String receiptClientId;
        String htmlUrl;
        String userIdS;
        String retailerIdS;
        boolean starred;
        String total;

        public String getIdS() {
            return idS;
        }

        public Long getCreatedAtI() {
            return createdAtI;
        }

        public Long getUpdatedAtI() {
            return updatedAtI;
        }

        public String getReceiptClientId() {
            return receiptClientId;
        }

        public String getReceiptHtmlUrl() {
            return htmlUrl;
        }

        public String getUserIdS() {
            return userIdS;
        }

        public String getRetailerIdS() {
            return retailerIdS;
        }

        public boolean isStarred() {
            return starred;
        }

        public Double getTotal() {
            if (total == null) {
                return null;
            }

            try {
                return Double.parseDouble(total);
            } catch (NumberFormatException ex) {
                return null;
            }
        }

        public Long getDeletedAtI() {
            return deletedAtI;
        }

        public String getHtmlUrl() {
            return htmlUrl;
        }

        public Long getReadAtI() {
            return readAtI;
        }
    }

    @GET("/receipts")
    List<Receipt> listReceipts(@Query("last_updated_at") Long lastUpdatedAt, @Query("page") Integer page);


    @GET("/receipts")
    List<Receipt> allReceipts(@Query("page") Integer page);

    @PUT("/receipts/{id}")
    Receipt updateReceipt(@Path("id") String receiptId, @Query("starred") Boolean starred, @Query("read") Boolean read);

    @PUT("/receipts/{id}")
    Receipt claimReceipt(@Path("id") String receiptId, @Query("user_id") String userId);

    @DELETE("/receipts/{id}")
    Receipt deleteReceipt(@Path("id") String receiptId);
}
