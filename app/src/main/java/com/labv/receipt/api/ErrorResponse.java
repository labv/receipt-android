package com.labv.receipt.api;

import com.google.gson.annotations.SerializedName;

/**
 * Created by edmundlo on 2015-05-20.
 */
public class ErrorResponse {
    public class Error {
        @SerializedName("code")
        public String code;
    }
    @SerializedName("error")
    public Error error;

    public String getErrorCode() {
       if (error == null)  {
           return null;
       }

       return error.code;
    }
}
