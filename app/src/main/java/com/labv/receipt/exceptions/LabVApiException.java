package com.labv.receipt.exceptions;

import android.util.Log;

import com.labv.receipt.api.ErrorResponse;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;

/**
 * Created by edmundlo on 2015-08-01.
 */
public class LabVApiException extends LabVException {
    private static HashMap<String, Class> ERROR_CODES = new HashMap<String, Class>() {{
        put("email_in_use_error", EmailInUseException.class);
        put("invalid_parameters_error", InvalidParametersException.class);
        put("invalid_credential_error", InvalidCredentialException.class);
    }};

    public LabVApiException(ErrorResponse response) {
        // Free to override
    }

    public static LabVApiException buildException(ErrorResponse error) {
        if (error == null) {
           return null;
        }

        try {
            String code = error.getErrorCode();
            if (code != null) {
                Class klass = ERROR_CODES.get(code);
                if (klass != null) {
                    Class[] arg = new Class[1];
                    arg[0] = ErrorResponse.class;
                    return (LabVApiException) klass.getDeclaredConstructor(arg).newInstance(error);
                }
            }
        } catch (NoSuchMethodException ex) {
            Log.e("LabVException", ex.toString());
        } catch (InvocationTargetException ex) {
            Log.e("LabVException", ex.toString());
        } catch (InstantiationException ex) {
            Log.e("LabVException", ex.toString());
        } catch (IllegalAccessException ex) {
            Log.e("LabVException", ex.toString());
        }


        return null;
    }
}
