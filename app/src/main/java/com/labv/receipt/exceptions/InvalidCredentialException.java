package com.labv.receipt.exceptions;

import com.labv.receipt.api.ErrorResponse;

/**
 * Created by edmundlo on 2015-05-14.
 */
public class InvalidCredentialException extends LabVApiException {
    public InvalidCredentialException(ErrorResponse response) {
        super(response);
    }
}
