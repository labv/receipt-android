package com.labv.receipt.exceptions;

import com.labv.receipt.api.ErrorResponse;

/**
 * Created by edmundlo on 2015-05-20.
 */
public class InvalidParametersException extends LabVApiException {
    public InvalidParametersException(ErrorResponse response) {
        super(response);
    }
}
