package com.labv.receipt.exceptions;

import com.labv.Main;
import com.labv.receipt.R;
import com.labv.receipt.api.ErrorResponse;

/**
 * Created by edmundlo on 2015-05-20.
 */
public class EmailInUseException extends LabVApiException {

    public EmailInUseException(ErrorResponse response) {
        super(response);
    }

    @Override
    public String toString() {
        return Main.getContext().getString(R.string.error_email_in_use);
    }
}
