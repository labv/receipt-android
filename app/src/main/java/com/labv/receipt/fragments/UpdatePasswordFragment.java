package com.labv.receipt.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.labv.receipt.R;
import com.labv.receipt.activities.MainActivity;
import com.labv.receipt.exceptions.InvalidCredentialException;
import com.labv.receipt.tasks.HandlerSupportedAsyncTask;
import com.labv.receipt.tasks.users.UpdatePasswordTask;

public class UpdatePasswordFragment extends LabvBaseFragment {

    private EditText mOldPasswordField;
    private EditText mNewPasswordField;
    private ProgressBar mSaving;
    private Button mSaveButton;
    private MainActivity mActivity;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment UserProfile.
     */
    public static UpdatePasswordFragment newInstance() {
        UpdatePasswordFragment fragment = new UpdatePasswordFragment();
        return fragment;
    }

    public UpdatePasswordFragment() {
        // Required empty public constructor
    }

    public String getTitle() {
        return "Change Password";
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private boolean isPasswordValid(String password) {
        return password.length() >= 8;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // create ContextThemeWrapper from the original Activity Context with the custom theme
        final Context contextThemeWrapper = new ContextThemeWrapper(getActivity(), R.style.Theme_AppCompat);

        // clone the inflater using the ContextThemeWrapper
        LayoutInflater localInflater = inflater.cloneInContext(contextThemeWrapper);

        // Inflate the layout for this fragment
        View view = localInflater.inflate(R.layout.fragment_update_password, container, false);
        mOldPasswordField = (EditText) view.findViewById(R.id.old_password_field);
        mNewPasswordField = (EditText) view.findViewById(R.id.new_password_field);
        mSaving = (ProgressBar) view.findViewById(R.id.update_password_progress);
        mSaveButton = (Button) view.findViewById(R.id.update_password_button);
        mSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveChanges();
            }
        });

        return view;
    }

    private void saveChanges() {
        String oldPassword = mOldPasswordField.getText().toString();
        String newPassword = mNewPasswordField.getText().toString();

        boolean cancel = false;

        // Check for a valid password
        if (!isPasswordValid(oldPassword)) {
            mOldPasswordField.setError(getString(R.string.error_invalid_password));
            mOldPasswordField.requestFocus();
            cancel = true;
        }

        if (!isPasswordValid(newPassword)) {
            mNewPasswordField.setError(getString(R.string.error_invalid_password));
            mNewPasswordField.requestFocus();
            cancel = true;
        }

        if (cancel) {
            return;
        }

        mSaving.setVisibility(View.VISIBLE);
        UpdatePasswordTask.updatePassword(
                mActivity.getSession(),
                oldPassword,
                newPassword, new HandlerSupportedAsyncTask.AsyncTaskCompletedHandler<Void>() {
                    @Override
                    public void onAsyncTaskSucceeded(Void v) {
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
                        if(imm.isAcceptingText()) { // verify if the soft keyboard is open
                            imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                        }

                        if (mActivity != null) {
                            mActivity.tryPopFragment();
                        }
                    }

                    @Override
                    public void onAsyncTaskFinished() {
                        super.onAsyncTaskFinished();
                        mSaving.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAsyncTaskFailed(Exception ex) {
                        super.onAsyncTaskFailed(ex);

                        if (ex instanceof InvalidCredentialException) {
                            mOldPasswordField.setError(getString(R.string.error_current_password));
                            mOldPasswordField.requestFocus();
                        } else {
                            Toast.makeText(getActivity().getApplicationContext(), "Update password has failed. : " + ex.toString(), Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mActivity = (MainActivity) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must be MainActivity");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivity = null;
    }
}
