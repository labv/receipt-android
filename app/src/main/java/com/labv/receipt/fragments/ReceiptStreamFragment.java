package com.labv.receipt.fragments;

import android.app.Activity;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.activeandroid.content.ContentProvider;
import com.labv.Main;
import com.labv.receipt.R;
import com.labv.receipt.activities.AuthenticatedActivityBase;
import com.labv.receipt.activities.MainActivity;
import com.labv.receipt.adapters.MainStreamCursorAdapter;
import com.labv.receipt.models.Receipt;
import com.labv.receipt.tasks.HandlerSupportedAsyncTask;
import com.labv.receipt.tasks.receipts.RemoveReceiptTask;
import com.labv.receipt.tasks.receipts.StarReceiptTask;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Large screen devices (such as tablets) are supported by replacing the ListView
 * with a GridView.
 * <p/>
 * interface.
 */
public class ReceiptStreamFragment extends LabvBaseFragment implements AbsListView.OnItemClickListener, SwipeRefreshLayout.OnRefreshListener, MainStreamCursorAdapter.OnStarClickedListener {
    public interface ReceiptStreamItemClickedListener {
        public void onReceiptClicked(String receiptId);
    }

    /**
     * The fragment's ListView
     */
    private AbsListView mListView;
    private SwipeRefreshLayout mRefreshView;
    private MainStreamCursorAdapter mAdapter;
    private View mView;
    private TextView mNotificationView;
    private String mNotificationMessage;
    private ReceiptStreamItemClickedListener mListener;
    private View mProgressBar = null;
    private static String STARRED_ONLY = "starred_only";

    public static ReceiptStreamFragment newInstance(boolean starredOnly) {
        ReceiptStreamFragment fragment = new ReceiptStreamFragment();
        Bundle args=new Bundle();

        args.putBoolean(STARRED_ONLY, starredOnly);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ReceiptStreamFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mAdapter = new MainStreamCursorAdapter(
                getActivity(),
                null,
                true,
                this
        );
    }

    public void setNotificationMessage(String message) {
        mNotificationMessage = message;
        if (mNotificationView != null) {
            if (mNotificationMessage != null) {
                mNotificationView.setText(mNotificationMessage);
                ((View)mNotificationView.getParent()).setVisibility(View.VISIBLE);
            } else {
                ((View)mNotificationView.getParent()).setVisibility(View.GONE);
            }
        }
    }

    public String getTitle() {
        return "My Receipts";
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_receipt_stream, container, false);
        mView = view;
        mNotificationView = (TextView) view.findViewById(R.id.notification_bar);

        setNotificationMessage(mNotificationMessage);

        // Set the adapter
        mListView = (AbsListView) view.findViewById(R.id.main_list);

        // Set OnItemClickListener so we can be notified on item clicks
        mListView.setOnItemClickListener(this);
        mListView.setAdapter(mAdapter);

        mRefreshView = (SwipeRefreshLayout) view.findViewById(R.id.refresh_view);
        mRefreshView.setOnRefreshListener(this);
        mRefreshView.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        mListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int topRowVerticalPosition =
                        (mListView == null || mListView.getChildCount() == 0) ?
                                0 : mListView.getChildAt(0).getTop();
                mRefreshView.setEnabled(firstVisibleItem == 0 && topRowVerticalPosition >= 0);
            }
        });


        registerForContextMenu(mListView);
        setupCursorLoader();
        return view;
    }

    private boolean getStarredOnly() {
        return (getArguments().getBoolean(STARRED_ONLY));
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (ReceiptStreamItemClickedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement ReceiptStreamItemClickedListener");
        }
    }



    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Cursor cursor = (Cursor)mListView.getAdapter().getItem(position);
        mListener.onReceiptClicked(cursor.getString(cursor.getColumnIndex("ReceiptId")));
    }

    public void setupCursorLoader() {
        getLoaderManager().initLoader(0, null, new LoaderManager.LoaderCallbacks<Cursor>() {
            @Override
            public Loader<Cursor> onCreateLoader(int id, Bundle args) {
                if (getStarredOnly()) {
                    String [] selectArgs = {"1"};
                    return new CursorLoader(getActivity(),
                            ContentProvider.createUri(Receipt.class, null),
                            null, "starred = ?", selectArgs, "CreatedDate DESC"
                    );
                } else {
                    return new CursorLoader(getActivity(),
                            ContentProvider.createUri(Receipt.class, null),
                            null, null, null, "CreatedDate DESC"
                    );
                }
            }

            @Override
            public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
                mAdapter.swapCursor(data);
            }

            @Override
            public void onLoaderReset(Loader<Cursor> loader) {
                mAdapter.swapCursor(null);
            }
        });
    }

    @Override
    public void onRefresh() {
        ((Main)getActivity().getApplication()).getAnalyticsService().firePullToRefreshEvent(baseActivity().getUser().getUserIdHash());
        ((MainActivity) getActivity()).updateFromServer(new HandlerSupportedAsyncTask.AsyncTaskCompletedHandler() {
            @Override
            public void onAsyncTaskFinished() {
                super.onAsyncTaskFinished();
                mRefreshView.setRefreshing(false);
            }
        });
    }

    @Override
    public void onStarClicked(String receiptId, ImageView view) {
        String state = (String) view.getTag();

        if (state.equals("empty")) {
            view.setImageResource(R.drawable.star);
            view.setTag("selected");
            StarReceiptTask.starReceipt(
                    ((AuthenticatedActivityBase) getActivity()).getSession(),
                    receiptId,
                    null
            );
        } else {
            view.setImageResource(R.drawable.empty_star);
            view.setTag(R.drawable.empty_star);
            view.setTag("empty");
            StarReceiptTask.unstarReceipt(
                    ((AuthenticatedActivityBase) getActivity()).getSession(),
                    receiptId,
                    null
            );
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if (v.getId() == R.id.main_list) {
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;
            Cursor cursor = mAdapter.getCursor();
            cursor.moveToPosition(info.position);
            String[] menuItems = null;
            if (cursor.getInt(cursor.getColumnIndex("Starred")) > 0) {
                menuItems = getResources().getStringArray(R.array.unstarred_menu_items);
            } else {
                menuItems = getResources().getStringArray(R.array.starred_menu_items);
            }

            for (int i = 0; i<menuItems.length; i++) {
                menu.add(Menu.NONE, i, i, menuItems[i]);
            }

            menu.add(Menu.NONE, menuItems.length, menuItems.length, cursor.getString(cursor.getColumnIndex("ReceiptId")));
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        int menuItemIndex = item.getItemId();
        Cursor cursor = mAdapter.getCursor();
        cursor.moveToPosition(info.position);

        String receiptId = cursor.getString(cursor.getColumnIndex("ReceiptId"));
        switch (menuItemIndex) {
            case 0:
                removeReceipt(receiptId);
                break;
            case 1:
                onStarClicked(receiptId, (ImageView)info.targetView.findViewById(R.id.favouriteButton));
                break;
        }

        return true;
    }

    private void removeReceipt(String receiptId) {
        if (mProgressBar == null) {
            mProgressBar = new RelativeLayout(getActivity());
            ProgressBar progressBar = new ProgressBar(getActivity(), null, android.R.attr.progressBarStyleLarge);
            progressBar.setIndeterminate(true);
            progressBar.setVisibility(View.VISIBLE);
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(100,100);
            params.addRule(RelativeLayout.CENTER_IN_PARENT);
            ((RelativeLayout) mProgressBar).addView(progressBar, params);
        }

        RemoveReceiptTask.removeReceipt(
                ((AuthenticatedActivityBase) getActivity()).getSession(),
                receiptId,
                new HandlerSupportedAsyncTask.AsyncTaskCompletedHandler() {
                    @Override
                    public void onAsyncTaskFinished() {
                        super.onAsyncTaskFinished();
                        mProgressBar.setVisibility(View.GONE);
                    }
                }
        );
    }
}

