package com.labv.receipt.fragments;

import android.app.Activity;
import android.database.ContentObserver;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.activeandroid.content.ContentProvider;
import com.labv.receipt.PreferenceManager;
import com.labv.receipt.R;
import com.labv.receipt.models.User;

import java.util.HashMap;
import java.util.Map;

public class LeftMenuFragment extends LabvBaseFragment implements AdapterView.OnItemClickListener {
    public static final String MY_RECEIPTS = "MyReceipts";
    public static final String STARRED = "Starred";
    public static final String PROMOTIONS = "Promotions";
    public static final String MY_PROFILE = "My Profile";
    public static final String CHANGE_PASSWORD = "Change Password";
    public static final String LOG_OUT = "Logout";

    private static final Map<Integer, String> ITEM_MAPS = new HashMap<Integer, String>() {{
        put(0, MY_RECEIPTS);
        put(1, STARRED);
        put(2, PROMOTIONS);
        put(3, MY_PROFILE);
        put(4, CHANGE_PASSWORD);
        put(5, LOG_OUT);
    }};

    public interface OnLeftMenuItemClicked {
        public void onItemClicked(final String view);
    }

    private ContentObserver mUserInfoChangedListener = null;
    private TextView mEmailTextView = null;
    private User mUser = null;
    private ArrayAdapter<String> mAdapter = null;
    private ListView mMenuListView = null;
    private TextView mReceiptCreditView = null;
    private OnLeftMenuItemClicked mListener;

    public LeftMenuFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mUser = User.find(PreferenceManager.getInstance().getSessionInfo().getUserId());

        if (getArguments() != null) {
            // TODO
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        mEmailTextView = (TextView)getView().findViewById(R.id.email_text_view);
        mMenuListView = (ListView)getView().findViewById(R.id.left_menu_list);
        registerUserChangedListener();
        updateUserInfo();
        setupMenu();
    }

    private void registerUserChangedListener() {
        mUserInfoChangedListener = new ContentObserver(new Handler()) {
            @Override
            public void onChange(boolean selfChange) {
                super.onChange(selfChange);
                mUser = User.find(PreferenceManager.getInstance().getSessionInfo().getUserId());
                updateUserInfo();
            }
        };

        getActivity().getContentResolver().registerContentObserver(
                ContentProvider.createUri(User.class, null), true, mUserInfoChangedListener
        );
    }

    private void unregisterListener() {
        if (mUserInfoChangedListener != null) {
            getActivity().getContentResolver().unregisterContentObserver(mUserInfoChangedListener);
            mUserInfoChangedListener = null;
        }
    }

    private void setupMenu() {
        String[] menuItems = getActivity().getResources().getStringArray(R.array.menu_items);
        String[] menuItemIcons = getActivity().getResources().getStringArray(R.array.integer_menu_icon);
        mAdapter = new ArrayAdapter<String>(
                getActivity(),
                android.R.layout.simple_list_item_1,
                menuItems
        );

        mMenuListView.setAdapter(mAdapter);
        mMenuListView.setOnItemClickListener(this);
    }

    private void updateUserInfo() {
        if (mUser != null) {
            mEmailTextView.setText(mUser.email);
            mReceiptCreditView.setText("" + mUser.receiptCount());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_left_menu, container, false);
        mReceiptCreditView = (TextView) view.findViewById(R.id.receipt_credit_count);
        return view;
    }

    @Override
    public void onStop() {
        super.onStop();
        unregisterListener();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnLeftMenuItemClicked) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        mListener.onItemClicked(ITEM_MAPS.get(position));
    }

    public String getTitle() {
        return "Menu";
    }
}
