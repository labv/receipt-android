package com.labv.receipt.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.ConsoleMessage;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.labv.Main;
import com.labv.receipt.R;
import com.labv.receipt.activities.AuthenticatedActivityBase;
import com.labv.receipt.activities.MainActivity;
import com.labv.receipt.models.Receipt;
import com.labv.receipt.tasks.receipts.ReadReceiptTask;

/**
 */
public class ReceiptDetailsFragment extends LabvBaseFragment {

    private static final String RECEIPT_ID = "RECEIPT_ID";

    private String receiptId;
    private Receipt mReceipt;
    private TextView mEmptyView;
    private WebView mWebView;
    private View mSpinnerView;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param receiptId Server receipt ID
     * @return A new instance of fragment ReceiptDetailsFragment.
     */
    public static ReceiptDetailsFragment newInstance(String receiptId) {
        ReceiptDetailsFragment fragment = new ReceiptDetailsFragment();
        Bundle args = new Bundle();
        args.putString(RECEIPT_ID, receiptId);
        fragment.receiptId = receiptId;
        fragment.setArguments(args);
        return fragment;
    }

    public ReceiptDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            receiptId = getArguments().getString(RECEIPT_ID);
        }
    }

    private Receipt getReceipt() {
        if (mReceipt == null) {
            mReceipt = Receipt.find(receiptId);
        }
        return mReceipt;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_receipt_details, container, false);
        mEmptyView = (TextView) view.findViewById(R.id.receipt_text_view);
        mWebView = (WebView) view.findViewById(R.id.receipt_web_view);
        mSpinnerView = (View) view.findViewById(R.id.spinner_view);
        mWebView.setInitialScale(1);
        mWebView.getSettings().setLoadWithOverviewMode(true);
        mWebView.getSettings().setUseWideViewPort(true);
        mWebView.getSettings().setSupportMultipleWindows(true);
        mWebView.getSettings().setDomStorageEnabled(true);
        mWebView.getSettings().setJavaScriptEnabled(true);

        if (getReceipt().htmlPath != null) {
            mEmptyView.setVisibility(View.GONE);
            mWebView.setVisibility(View.GONE);
            mWebView.setWebChromeClient(new WebChromeClient() {
                @Override
                public boolean onConsoleMessage(ConsoleMessage cm) {
                    Log.i("ReceiptWebView", cm.message() + " -- From line "
                            + cm.lineNumber() + " of "
                            + cm.sourceId());
                    return super.onConsoleMessage(cm);
                }
            });
            mWebView.setWebViewClient(new WebViewClient() {
                @Override
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(view, url);
                    mWebView.setVisibility(View.VISIBLE);
                    mSpinnerView.setVisibility(View.GONE);
                }
            });
            mWebView.loadUrl(getReceipt().getFullHtmlPath());
        } else {
            mWebView.setVisibility(View.GONE);
        }

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        AuthenticatedActivityBase authActivity = (AuthenticatedActivityBase) activity;
        ReadReceiptTask.readReceipt(((MainActivity) activity).getSession(), receiptId, null);
        ((Main)activity.getApplication()).getAnalyticsService().fireReceiptView(authActivity.getUser().getUserIdHash(), receiptId);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public String getTitle() {
        return getReceipt().retailer.getName();
    }
}
