package com.labv.receipt.fragments;


import android.app.Activity;
import android.app.Fragment;

import com.google.android.gms.analytics.HitBuilders;
import com.labv.Main;
import com.labv.receipt.activities.AuthenticatedActivityBase;

/**
 * Created by edmundlo on 2015-06-06.
 */
public abstract class LabvBaseFragment extends Fragment {
    public abstract String getTitle();

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((Main)activity.getApplication()).getDefaultTracker().setScreenName(getTitle());
        ((Main)activity.getApplication()).getDefaultTracker().send(new HitBuilders.ScreenViewBuilder().build());
    }

    public AuthenticatedActivityBase baseActivity() {
        AuthenticatedActivityBase authActivity = (AuthenticatedActivityBase) getActivity();
        return authActivity;
    }
}
