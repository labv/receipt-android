package com.labv.receipt.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.labv.receipt.R;
import com.labv.receipt.activities.MainActivity;
import com.labv.receipt.exceptions.EmailInUseException;
import com.labv.receipt.models.User;
import com.labv.receipt.tasks.HandlerSupportedAsyncTask;
import com.labv.receipt.tasks.users.UpdateProfileTask;

public class UserProfileFragment extends LabvBaseFragment {

    private EditText mFirstNameField;
    private EditText mLastNameField;
    private AutoCompleteTextView mEmailField;
    private ProgressBar mSaving;
    private Button mSaveButton;
    private String mEmail;
    private String mLastName;
    private String mFirstName;
    private MainActivity mActivity;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment UserProfile.
     */
    public static UserProfileFragment newInstance(User user) {
        UserProfileFragment fragment = new UserProfileFragment();
        Bundle args = new Bundle();
        args.putString("email", user.email);
        args.putString("lastName", user.lastName);
        args.putString("firstName", user.firstName);
        fragment.setArguments(args);
        return fragment;
    }

    public UserProfileFragment() {
        // Required empty public constructor
    }

    public String getTitle() {
        return "User Profile";
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mEmail = getArguments().getString("email");
            mLastName = getArguments().getString("lastName");
            mFirstName = getArguments().getString("firstName");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // create ContextThemeWrapper from the original Activity Context with the custom theme
        final Context contextThemeWrapper = new ContextThemeWrapper(getActivity(), R.style.Theme_AppCompat);

        // clone the inflater using the ContextThemeWrapper
        LayoutInflater localInflater = inflater.cloneInContext(contextThemeWrapper);

        // Inflate the layout for this fragment
        View view = localInflater.inflate(R.layout.fragment_user_profile, container, false);
        mEmailField = (AutoCompleteTextView) view.findViewById(R.id.email_field);
        mFirstNameField = (EditText) view.findViewById(R.id.first_name_field);
        mLastNameField = (EditText) view.findViewById(R.id.last_name_field);
        mEmailField.setText(mEmail);
        mFirstNameField.setText(mFirstName);
        mLastNameField.setText(mLastName);
        mSaving = (ProgressBar) view.findViewById(R.id.update_user_progress);
        mSaveButton = (Button) view.findViewById(R.id.update_user_button);
        mSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveChanges();
            }
        });

        return view;
    }

    private boolean isEmailValid(String email) {
        return email.contains("@");
    }

    private void saveChanges() {
        String newEmail = mEmailField.getText().toString();
        String newFirstName = mFirstNameField.getText().toString();
        String newLastName = mLastNameField.getText().toString();

        boolean cancel = false;

        // Check for a valid email address.
        if (TextUtils.isEmpty(newEmail)) {
            mEmailField.setError(getString(R.string.error_field_required));
            mEmailField.requestFocus();
            cancel = true;
        } else if (!isEmailValid(newEmail)) {
            mEmailField.setError(getString(R.string.error_invalid_email));
            mEmailField.requestFocus();
            cancel = true;
        }

        if (cancel) {
            return;
        }

        if (!newEmail.equals(mEmail) || !newFirstName.equals(mFirstName) || !newLastName.equals(mLastName)) {
            mSaving.setVisibility(View.VISIBLE);
            UpdateProfileTask.updateProfile(
                    mActivity.getSession(),
                    newFirstName,
                    newLastName,
                    newEmail, new HandlerSupportedAsyncTask.AsyncTaskCompletedHandler<Void>() {
                        @Override
                        public void onAsyncTaskSucceeded(Void v) {
                            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
                            if(imm.isAcceptingText()) { // verify if the soft keyboard is open
                                imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                            }

                            if (mActivity != null) {
                                mActivity.tryPopFragment();
                            }
                        }

                        @Override
                        public void onAsyncTaskFinished() {
                            super.onAsyncTaskFinished();
                            mSaving.setVisibility(View.GONE);
                        }

                        @Override
                        public void onAsyncTaskFailed(Exception ex) {
                            super.onAsyncTaskFailed(ex);

                            if (ex instanceof EmailInUseException) {
                                mEmailField.setError(getString(R.string.error_email_in_use));
                                mEmailField.requestFocus();
                            } else {
                                Toast.makeText(getActivity().getApplicationContext(), "Failed to login: " + ex.toString(), Toast.LENGTH_LONG).show();
                            }
                        }
                    });
        } else {
            if (mActivity != null) {
                mActivity.tryPopFragment();
            }
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mActivity = (MainActivity) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement ReceiptStreamItemClickedListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivity = null;
    }
}
