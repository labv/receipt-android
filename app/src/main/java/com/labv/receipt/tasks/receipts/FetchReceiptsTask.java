package com.labv.receipt.tasks.receipts;

import android.util.Log;

import com.labv.receipt.api.ApiFactory;
import com.labv.receipt.api.ReceiptApi;
import com.labv.receipt.models.Receipt;
import com.labv.receipt.models.Session;
import com.squareup.okhttp.OkHttpClient;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by edmundlo on 2015-05-13.
 */
public class FetchReceiptsTask extends FetchReceiptTask<Void, Void, List<Receipt>> {
    private static final Object lock = new Object();
    private static FetchReceiptsTask mTask = null;
    private OkHttpClient client = null;

    public static FetchReceiptsTask fetchNewReceipts(Session session, AsyncTaskCompletedHandler handler) {
        synchronized (lock) {
            if (mTask == null) {
                mTask = new FetchReceiptsTask(session);
                mTask.addAsyncTaskCompletedHandler(new AsyncTaskCompletedHandler<List<Receipt>>() {
                    @Override
                    public void onAsyncTaskFinished() {
                        super.onAsyncTaskFinished();
                        synchronized (lock) {
                            mTask = null;
                        }
                    }

                    @Override
                    public void onAsyncTaskFailed(Exception ex) {
                        super.onAsyncTaskFailed(ex);
                        Log.e("FetchReceiptsTask", ex.toString());
                    }
                });

                if (handler != null) {
                    mTask.addAsyncTaskCompletedHandler(handler);
                }

                mTask.execute();
            } else {
                Log.i("FetchReceiptsTask", "Fetch in progress, returning existing one");
            }
            return mTask;
        }
    }

    private FetchReceiptsTask(Session session) {
        super(session);
    }

    @Override
    protected List<Receipt> runTask(Void... params) throws Exception {
        ReceiptApi receiptApi = ApiFactory.getInstance(ReceiptApi.class).buildService(getSession());
        List<Receipt> receipts = new ArrayList<Receipt>();

        Receipt latestReceipt = Receipt.latest();
        Long lastUpdatedAt = null;

        if (latestReceipt != null) {
            lastUpdatedAt = latestReceipt.updatedDate.getTime() / 1000;
        }

        List<ReceiptApi.Receipt> page;
        Integer pageNumber = 1;
        do {
            if (lastUpdatedAt != null) {
                page = receiptApi.listReceipts(lastUpdatedAt, pageNumber);
            } else {
                page = receiptApi.allReceipts(pageNumber);
            }

            receipts.addAll(processPage(page));

            pageNumber++;
        } while(page.size() != 0);

        return receipts;
    }

    private List<Receipt> processPage(List<ReceiptApi.Receipt> page) {
        List<Receipt> receipts = new ArrayList<>();

        for(ReceiptApi.Receipt apiObject : page) {
            Receipt local = findOrBuildReceiptFromApiObject(apiObject);
            if (local != null) {
                receipts.add(local);
            }
        }

        return receipts;
    }
}

