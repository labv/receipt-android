package com.labv.receipt.tasks.sessions;

import com.labv.receipt.PreferenceManager;
import com.labv.receipt.api.ApiFactory;
import com.labv.receipt.api.SessionApi;
import com.labv.receipt.models.Session;
import com.labv.receipt.tasks.AuthenticatedHandlerSupportedAsyncTask;

/**
 * Created by edmundlo on 2015-05-13.
 */
public class LogoutTask extends AuthenticatedHandlerSupportedAsyncTask<Void, Void, Void> {
    private Session mSession;

    public LogoutTask(Session session) {
        super(session);
        this.mSession = session;
    }

    @Override
    protected Void runTask(Void... params) throws Exception {
        SessionApi sessionApi = ApiFactory.getInstance(SessionApi.class).buildService(mSession);
        sessionApi.destroy();
        return (Void) null;
    }

    @Override
    protected Void runAfterInForeground(Void aVoid) {
        PreferenceManager.getInstance().removeSessionInfo(mSession);
        return super.runAfterInForeground(aVoid);
    }
}
