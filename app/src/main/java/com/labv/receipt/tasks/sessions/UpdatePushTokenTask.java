package com.labv.receipt.tasks.sessions;

import android.util.Log;

import com.labv.receipt.PreferenceManager;
import com.labv.receipt.api.ApiFactory;
import com.labv.receipt.api.SessionApi;
import com.labv.receipt.models.Session;
import com.labv.receipt.tasks.AuthenticatedHandlerSupportedAsyncTask;

/**
 * Created by edmundlo on 2015-05-13.
 */
public class UpdatePushTokenTask extends AuthenticatedHandlerSupportedAsyncTask<Void, Void, Void> {

    private String token;

    public UpdatePushTokenTask(Session mSession, String token) {
        super(mSession);
        this.token = token;
    }

    @Override
    protected Void runTask(Void... params) throws Exception {
        Log.d("UpdatePushTokenTask", String.format("Saving push token %s", token));
        SessionApi sessionApi = ApiFactory.getInstance(SessionApi.class).buildService(getSession());
        SessionApi.Session apiSession = sessionApi.update(token);

        return (Void) null;
    }

    @Override
    protected Void runAfterSuccessInForeground(Void aVoid) {
        PreferenceManager.getInstance().setPushToken(token);
        return super.runAfterSuccessInForeground(aVoid);
    }
}
