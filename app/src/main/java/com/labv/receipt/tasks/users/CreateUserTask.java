package com.labv.receipt.tasks.users;

import com.labv.receipt.PreferenceManager;
import com.labv.receipt.api.ApiFactory;
import com.labv.receipt.api.SessionApi;
import com.labv.receipt.api.UserApi;
import com.labv.receipt.models.Session;
import com.labv.receipt.tasks.HandlerSupportedAsyncTask;

/**
 * Created by edmundlo on 2015-05-13.
 */
public class CreateUserTask extends HandlerSupportedAsyncTask<Void, Void, Session> {
    private String mFirstName;
    private String mLastName;
    private String mPassword;
    private String mEmail;

    private static final String DEVICE_TYPE = "android";

    public CreateUserTask(String firstName, String lastName, String password, String email) {
        this.mFirstName = firstName;
        this.mLastName = lastName;
        this.mPassword = password;
        this.mEmail = email;
    }

    @Override
    protected Session runTask(Void... params) throws Exception {
        UserApi userApi = ApiFactory.getInstance(UserApi.class).buildService();

        UserApi.User user = userApi.create(mFirstName, mLastName, mPassword, DEVICE_TYPE, mEmail, true);
        SessionApi.Session apiSession = user.getSession();

        Session session = new Session(apiSession.getSessionToken(), apiSession.getIdS(), apiSession.getUserIdS());
        PreferenceManager.getInstance().setSessionInfo(session);

        return session;
    }
}
