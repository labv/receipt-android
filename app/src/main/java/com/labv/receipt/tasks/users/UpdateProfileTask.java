package com.labv.receipt.tasks.users;

import com.labv.receipt.api.ApiFactory;
import com.labv.receipt.api.UserApi;
import com.labv.receipt.models.Session;
import com.labv.receipt.models.User;
import com.labv.receipt.tasks.AuthenticatedHandlerSupportedAsyncTask;

/**
 * Created by edmundlo on 2015-05-13.
 */
public class UpdateProfileTask extends AuthenticatedHandlerSupportedAsyncTask<Void, Void, Void> {
    private static final Object lock = new Object();
    private static UpdateProfileTask runningTask;

    private String mFirstName;
    private String mLastName;
    private String mEmail;

    public static UpdateProfileTask updateProfile(
            Session session,
            String firstName,
            String lastName,
            String email,
            AsyncTaskCompletedHandler handler) {

        synchronized (lock) {
            if (runningTask == null) {
                runningTask = new UpdateProfileTask(
                        session,
                        firstName,
                        lastName,
                        email);
                runningTask.addAsyncTaskCompletedHandler(new AsyncTaskCompletedHandler<Void>() {
                    @Override
                    public void onAsyncTaskFinished() {
                        super.onAsyncTaskFinished();

                        synchronized (lock) {
                            runningTask = null;
                        }
                    }
                });

                if (handler != null) {
                    runningTask.addAsyncTaskCompletedHandler(handler);
                }

                runningTask.execute();
            } else {
                if (handler != null) {
                    runningTask.addAsyncTaskCompletedHandler(handler);
                }
            }
        }

        return runningTask;
    }

    private UpdateProfileTask(
            Session session,
            String firstName,
            String lastName,
            String email) {
        super(session);
        mFirstName = firstName;
        mLastName = lastName;
        mEmail = email;
    }

    @Override
    protected Void runTask(Void... params) throws Exception {
        UserApi userApi = ApiFactory.getInstance(UserApi.class).buildService(getSession());

        User u = User.currentUser();
        userApi.update(getSession().getUserId(), mFirstName, mLastName, mEmail);
        u.firstName = mFirstName;
        u.lastName = mLastName;
        u.email = mEmail;
        u.save();

        return null;
    }
}

