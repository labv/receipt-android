package com.labv.receipt.tasks.receipts;

import com.labv.receipt.api.ApiFactory;
import com.labv.receipt.api.ReceiptApi;
import com.labv.receipt.models.Receipt;
import com.labv.receipt.models.Session;
import com.labv.receipt.tasks.AuthenticatedHandlerSupportedAsyncTask;
import com.squareup.okhttp.OkHttpClient;

import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by edmundlo on 2015-05-13.
 */
public class ReadReceiptTask extends AuthenticatedHandlerSupportedAsyncTask<Void, Void, Receipt> {
    private OkHttpClient client = null;
    private boolean mRead = false;
    private String mReceiptId = null;
    private static final Map<String, ReadReceiptTask> runningTasks = new ConcurrentHashMap<>();

    public static ReadReceiptTask unreadReceipt(Session session, String receiptId, AsyncTaskCompletedHandler handler) {
        return handleReceipt(session, receiptId, false, handler);
    }

    public static ReadReceiptTask readReceipt(Session session, String receiptId, AsyncTaskCompletedHandler handler) {
        return handleReceipt(session, receiptId, true, handler);
    }

    private static ReadReceiptTask handleReceipt(Session session, final String receiptId, boolean read, AsyncTaskCompletedHandler handler) {
        ReadReceiptTask task;

        synchronized (runningTasks) {
            task = runningTasks.get(receiptId);
            if (task == null) {
                task = new ReadReceiptTask(session, receiptId, read);
                task.addAsyncTaskCompletedHandler(new AsyncTaskCompletedHandler<Receipt>() {
                    @Override
                    public void onAsyncTaskFinished() {
                        super.onAsyncTaskFinished();

                        synchronized (runningTasks) {
                            runningTasks.remove(receiptId);
                        }
                    }
                });

                if (handler != null) {
                    task.addAsyncTaskCompletedHandler(handler);
                }
                task.execute();
            }
        }

        return task;
    }

    private ReadReceiptTask(Session session, String receiptId, boolean read) {
        super(session);
        mReceiptId = receiptId;
        mRead = read;
    }

    @Override
    protected Receipt runTask(Void... params) throws Exception {
        Receipt receipt = Receipt.find(mReceiptId);
        boolean changed = false;
        if (mRead) {
            if (receipt.readAt == null) {
                changed = true;
                receipt.readAt = new Date();
            }
        } else {
            if (receipt.readAt != null) {
                changed = true;
                receipt.readAt = null;
            }
        }

        receipt.save();

        if (changed) {
            ReceiptApi receiptApi = ApiFactory.getInstance(ReceiptApi.class).buildService(getSession());
            receiptApi.updateReceipt(mReceiptId, null, mRead);
        }

        return receipt;
    }
}

