package com.labv.receipt.tasks.receipts;

import android.util.Log;

import com.labv.receipt.api.ApiFactory;
import com.labv.receipt.api.ReceiptApi;
import com.labv.receipt.models.Receipt;
import com.labv.receipt.models.Retailer;
import com.labv.receipt.models.Session;
import com.labv.receipt.tasks.AuthenticatedHandlerSupportedAsyncTask;
import com.labv.receipt.tasks.retailers.FetchRetailerTask;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

/**
 * Created by edmundlo on 2015-05-13.
 */
public abstract class FetchReceiptTask<P, G, R> extends AuthenticatedHandlerSupportedAsyncTask<P, G, R> {
    private OkHttpClient httpClient;

    public FetchReceiptTask(Session session) {
        super(session);
    }

    protected Receipt findOrBuildReceiptFromApiObject (ReceiptApi.Receipt apiObject) {
        Receipt local = Receipt.find(apiObject.getIdS());
        if (apiObject.getDeletedAtI() != null) {
            if (local != null) {
                local.delete();
            }
            return local;
        }

        if (local == null) {
            local = new Receipt();
        }

        if ((local.htmlPath != null || apiObject.getHtmlUrl() == null) && local.updatedDate.equals(new Date(apiObject.getUpdatedAtI() * 1000))) {
            return local;
        }

        Log.i("FetchReceiptsTask", "Fetching new content " + apiObject.getIdS());

        local.receiptId = apiObject.getIdS();
        local.receiptClientId = apiObject.getReceiptClientId();
        local.createdDate = new Date(apiObject.getCreatedAtI() * 1000);
        local.updatedDate = new Date(apiObject.getUpdatedAtI() * 1000);
        local.starred = apiObject.isStarred();
        local.total = apiObject.getTotal();

        if (apiObject.getReadAtI() != null) {
            local.readAt = new Date(apiObject.getReadAtI() * 1000);
        } else {
            local.readAt = null;
        }

        local.retailer = Retailer.findOrCreate(apiObject.getRetailerIdS());

        FetchRetailerTask.fetchRetailer(getSession(), local.retailer, null);

        if (apiObject.getReceiptHtmlUrl() != null) {
            try {
                InputStream stream = fetchHtmlContent(apiObject.getReceiptHtmlUrl());
                try {
                    local.saveHtmlString(stream);
                } catch (Exception ex) {
                    Log.e("FetchReceiptsTask", "Failed to fetch html content" + ex.toString());
                } finally {
                    stream.close();
                }
            } catch (IOException ex) {
            }
        }

        local.save();

        Log.i("FetchReceiptsTask", "Finished fetching new content " + apiObject.getIdS());

        return local;
    }

    private InputStream fetchHtmlContent(String path) throws IOException {
        if (httpClient == null) {
            httpClient = new OkHttpClient();
        }

        Request request = new Request.Builder()
                .addHeader(
                        "Authorization",
                        buildSessionHeader(
                                getSession().getUserId(),
                                getSession().getSessionId(),
                                getSession().getSessionToken()
                        )
                ).url(ApiFactory.getInstance(ReceiptApi.class).buildUrl(path))
                .build();

        Response response = httpClient.newCall(request).execute();
        return response.body().byteStream();
    }

    private String buildSessionHeader(String userId, String sessionId, String sessionToken) {
        return String.format("ApiToken uid=%s, sid=%s, token=%s", userId, sessionId, sessionToken);
    }
}

