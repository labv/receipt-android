package com.labv.receipt.tasks.receipts;

import com.labv.receipt.api.ApiFactory;
import com.labv.receipt.api.ReceiptApi;
import com.labv.receipt.models.Receipt;
import com.labv.receipt.models.Session;
import com.labv.receipt.tasks.AuthenticatedHandlerSupportedAsyncTask;
import com.squareup.okhttp.OkHttpClient;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by edmundlo on 2015-05-13.
 */
public class StarReceiptTask extends AuthenticatedHandlerSupportedAsyncTask<Void, Void, Receipt> {
    private OkHttpClient client = null;
    private boolean mStar = false;
    private String mReceiptId = null;
    private static final Map<String, StarReceiptTask> runningTasks = new ConcurrentHashMap<>();

    public static StarReceiptTask unstarReceipt(Session session, String receiptId, AsyncTaskCompletedHandler handler) {
        return handleReceipt(session, receiptId, false, handler);
    }

    public static StarReceiptTask starReceipt(Session session, String receiptId, AsyncTaskCompletedHandler handler) {
        return handleReceipt(session, receiptId, true, handler);
    }

    private static StarReceiptTask handleReceipt(Session session, final String receiptId, boolean star, AsyncTaskCompletedHandler handler) {
        StarReceiptTask task;

        synchronized (runningTasks) {
            task = runningTasks.get(receiptId);
            if (task == null) {
                task = new StarReceiptTask(session, receiptId, star);
                task.addAsyncTaskCompletedHandler(new AsyncTaskCompletedHandler<Receipt>() {
                    @Override
                    public void onAsyncTaskFinished() {
                        super.onAsyncTaskFinished();

                        synchronized (runningTasks) {
                            runningTasks.remove(receiptId);
                        }
                    }
                });

                if (handler != null) {
                    task.addAsyncTaskCompletedHandler(handler);
                }
                task.execute();
            }
        }

        return task;
    }

    private StarReceiptTask(Session session, String receiptId, boolean star) {
        super(session);
        mReceiptId = receiptId;
        mStar = star;
    }

    @Override
    protected Receipt runTask(Void... params) throws Exception {
        ReceiptApi receiptApi = ApiFactory.getInstance(ReceiptApi.class).buildService(getSession());
        Receipt receipt = Receipt.find(mReceiptId);

        receipt.starred = mStar;
        receipt.save();

        receiptApi.updateReceipt(mReceiptId, mStar, null);

        return receipt;
    }
}

