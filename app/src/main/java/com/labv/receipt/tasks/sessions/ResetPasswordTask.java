package com.labv.receipt.tasks.sessions;

import com.labv.receipt.api.ApiFactory;
import com.labv.receipt.api.SessionApi;
import com.labv.receipt.tasks.HandlerSupportedAsyncTask;

/**
 * Created by edmundlo on 2015-05-13.
 */
public class ResetPasswordTask extends HandlerSupportedAsyncTask<Void, Void, Void> {

    private String email;

    public ResetPasswordTask(String email) {
        this.email = email;
    }

    @Override
    protected Void runTask(Void... params) throws Exception {
        SessionApi sessionApi = ApiFactory.getInstance(SessionApi.class).buildService();
        sessionApi.resetPassword(email);

        return (Void) null;
    }
}
