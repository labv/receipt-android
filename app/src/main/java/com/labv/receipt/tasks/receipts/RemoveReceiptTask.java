package com.labv.receipt.tasks.receipts;

import com.labv.Main;
import com.labv.receipt.api.ApiFactory;
import com.labv.receipt.api.ReceiptApi;
import com.labv.receipt.models.Receipt;
import com.labv.receipt.models.Session;
import com.labv.receipt.tasks.AuthenticatedHandlerSupportedAsyncTask;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by edmundlo on 2015-05-13.
 */
public class RemoveReceiptTask extends AuthenticatedHandlerSupportedAsyncTask<Void, Void, Void> {
    private String mReceiptId = null;
    private static final Map<String, RemoveReceiptTask> runningTasks = new ConcurrentHashMap<>();

    public static RemoveReceiptTask removeReceipt(Session session, final String receiptId, AsyncTaskCompletedHandler handler) {
        RemoveReceiptTask task;

        synchronized (runningTasks) {
            task = runningTasks.get(receiptId);
            if (task == null) {
                task = new RemoveReceiptTask(session, receiptId);
                task.addAsyncTaskCompletedHandler(new AsyncTaskCompletedHandler<Void>() {
                    @Override
                    public void onAsyncTaskFinished() {
                        super.onAsyncTaskFinished();

                        synchronized (runningTasks) {
                            runningTasks.remove(receiptId);
                        }
                    }
                });

                if (handler != null) {
                    task.addAsyncTaskCompletedHandler(handler);
                }
                task.execute();
            }
        }

        return task;
    }

    private RemoveReceiptTask(Session session, String receiptId) {
        super(session);
        mReceiptId = receiptId;
    }

    @Override
    protected Void runTask(Void... params) throws Exception {
        ReceiptApi receiptApi = ApiFactory.getInstance(ReceiptApi.class).buildService(getSession());
        Receipt receipt = Receipt.find(mReceiptId);

        receiptApi.deleteReceipt(receipt.receiptId);
        if (receipt.htmlPath != null) {
            Main.getContext().deleteFile(receipt.htmlPath);
        }
        receipt.delete();

        return null;
    }
}

