package com.labv.receipt.tasks.retailers;

import android.util.Log;

import com.labv.receipt.api.ApiFactory;
import com.labv.receipt.api.RetailerApi;
import com.labv.receipt.models.Retailer;
import com.labv.receipt.models.Session;
import com.labv.receipt.tasks.AuthenticatedHandlerSupportedAsyncTask;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by edmundlo on 2015-05-13.
 */
public class FetchRetailerTask extends AuthenticatedHandlerSupportedAsyncTask<Void, Void, Retailer> {
    private static final long UPDATE_PERIOD = 24 * 60 * 60 * 1000; // 1 day
    private static final Map<String, FetchRetailerTask> runningTasks = new ConcurrentHashMap<>();

    private String mRetailerId;

    public static void updateRetailers(Session session) {
        List<Retailer> retailers = Retailer.all();
        for(Retailer retailer : retailers) {
            fetchRetailer(session, retailer, null);
        }
    }

    public static FetchRetailerTask fetchRetailer(Session session, final Retailer retailer, AsyncTaskCompletedHandler handler) {
        synchronized (runningTasks) {
            FetchRetailerTask task = null;
            if (task == null) {
                if (retailer.lastFetchedAt == null ||
                        Math.abs(retailer.lastFetchedAt.getTime() - new Date().getTime()) > UPDATE_PERIOD) {
                    task = new FetchRetailerTask(session, retailer.getRetailerId());
                    task.addAsyncTaskCompletedHandler(new AsyncTaskCompletedHandler<Retailer>() {
                        @Override
                        public void onAsyncTaskFinished() {
                            super.onAsyncTaskFinished();
                            synchronized (runningTasks) {
                                runningTasks.remove(retailer.getRetailerId());
                            }
                        }

                        @Override
                        public void onAsyncTaskFailed(Exception ex) {
                            super.onAsyncTaskFailed(ex);
                            Log.e("FetchRetailerTask", ex.toString());
                        }
                    });

                    if (handler != null) {
                        task.addAsyncTaskCompletedHandler(handler);
                    }

                    runningTasks.put(retailer.getRetailerId(), task);

                    task.execute();
                }
            } else {
                Log.i("FetchRetailerTask", "Fetch in progress, returning existing one");
            }
            return task;
        }
    }

    private FetchRetailerTask(Session session, String retailerId) {
        super(session);
        mRetailerId = retailerId;
    }

    @Override
    protected Retailer runTask(Void... params) throws Exception {
        Log.i("FetchRetailerTask", "Begin Fetching " + mRetailerId);
        RetailerApi retailerApi = ApiFactory.getInstance(RetailerApi.class).buildService(getSession());
        Retailer retailer = Retailer.findOrCreate(mRetailerId);
        RetailerApi.Retailer object = retailerApi.get(mRetailerId);

        if (object != null) {
            retailer.lastFetchedAt = new Date();
            retailer.logoImageUrl = object.getLogoImageUrl();
            retailer.name = object.getName();
            retailer.category = object.getCategory();
            retailer.save();
        }

        Log.i("FetchRetailerTask", "Finish Fetching " + mRetailerId);
        return retailer;
    }
}

