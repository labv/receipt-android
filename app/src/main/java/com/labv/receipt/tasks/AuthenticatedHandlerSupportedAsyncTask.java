package com.labv.receipt.tasks;

import com.labv.receipt.PreferenceManager;
import com.labv.receipt.models.Session;

import retrofit.RetrofitError;

public abstract class AuthenticatedHandlerSupportedAsyncTask<P, G, R>
        extends HandlerSupportedAsyncTask<P, G, R> {

    protected Session mSession;

    protected AuthenticatedHandlerSupportedAsyncTask(Session mSession) {
        this.mSession = mSession;
    }

    protected Session getSession() {
        return mSession;
    }

    @Override
    protected AsyncTaskResponse<R> doInBackground(P... params) {
        AsyncTaskResponse response =  super.doInBackground(params);

        if (response.exception != null && response.exception instanceof RetrofitError) {
            RetrofitError retroError = (RetrofitError) response.exception;
            if (retroError.getResponse() != null && retroError.getResponse().getStatus() == 401) {
                invalidateSession();
            }
        }

        return response;
    }

    protected void invalidateSession() {
        if (mSession != null) {
            PreferenceManager.getInstance().removeSessionInfo(mSession);
        }
    }
}
