package com.labv.receipt.tasks.users;

import com.labv.receipt.api.ApiFactory;
import com.labv.receipt.api.UserApi;
import com.labv.receipt.models.Session;
import com.labv.receipt.tasks.AuthenticatedHandlerSupportedAsyncTask;

/**
 * Created by edmundlo on 2015-05-13.
 */
public class UpdatePasswordTask extends AuthenticatedHandlerSupportedAsyncTask<Void, Void, Void> {
    private static final Object lock = new Object();
    private static UpdatePasswordTask runningTask;

    private String mOldPassword;
    private String mNewPassword;

    public static UpdatePasswordTask updatePassword(
            Session session,
            String oldPassword,
            String newPassword,
            AsyncTaskCompletedHandler handler) {

        synchronized (lock) {
            if (runningTask == null) {
                runningTask = new UpdatePasswordTask(
                        session,
                        oldPassword,
                        newPassword
                );

                runningTask.addAsyncTaskCompletedHandler(new AsyncTaskCompletedHandler<Void>() {
                    @Override
                    public void onAsyncTaskFinished() {
                        super.onAsyncTaskFinished();

                        synchronized (lock) {
                            runningTask = null;
                        }
                    }
                });

                if (handler != null) {
                    runningTask.addAsyncTaskCompletedHandler(handler);
                }

                runningTask.execute();
            } else {
                if (handler != null) {
                    runningTask.addAsyncTaskCompletedHandler(handler);
                }
            }
        }

        return runningTask;
    }

    private UpdatePasswordTask(
            Session session,
            String oldPassword,
            String newPassword) {
        super(session);

        mOldPassword = oldPassword;
        mNewPassword = newPassword;
    }

    @Override
    protected Void runTask(Void... params) throws Exception {
        UserApi userApi = ApiFactory.getInstance(UserApi.class).buildService(getSession());

        userApi.updatePassword(getSession().getUserId(), mOldPassword, mNewPassword);

        return null;
    }
}

