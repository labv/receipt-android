package com.labv.receipt.tasks.sessions;

import com.labv.Main;
import com.labv.receipt.PreferenceManager;
import com.labv.receipt.api.ApiFactory;
import com.labv.receipt.api.SessionApi;
import com.labv.receipt.models.Session;
import com.labv.receipt.tasks.HandlerSupportedAsyncTask;

/**
 * Created by edmundlo on 2015-05-13.
 */
public class LoginTask extends HandlerSupportedAsyncTask<Void, Void, Session> {

    private String email;
    private String password;
    private static final String DEVICE_TYPE = "android";

    public LoginTask(String email, String password) {
        this.email = email;
        this.password = password;
    }

    @Override
    protected Session runTask(Void... params) throws Exception {
        SessionApi sessionApi = ApiFactory.getInstance(SessionApi.class).buildService();
        SessionApi.Session apiSession = sessionApi.create(email, password, Main.getDeviceId(), DEVICE_TYPE);

        Session session = new Session(apiSession.getSessionToken(), apiSession.getIdS(), apiSession.getUserIdS());
        PreferenceManager.getInstance().setSessionInfo(session);

        return session;
    }
}
