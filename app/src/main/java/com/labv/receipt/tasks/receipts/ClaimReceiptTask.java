package com.labv.receipt.tasks.receipts;

import com.labv.receipt.api.ApiFactory;
import com.labv.receipt.api.ReceiptApi;
import com.labv.receipt.models.Receipt;
import com.labv.receipt.models.Session;
import com.squareup.okhttp.OkHttpClient;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by edmundlo on 2015-05-13.
 */
public class ClaimReceiptTask extends FetchReceiptTask<Void, Void, Receipt> {

    private OkHttpClient client = null;
    private String mReceiptId = null;
    private static final Map<String, ClaimReceiptTask> runningTasks = new ConcurrentHashMap<>();

    public static ClaimReceiptTask claimReceipt(Session session, String receiptId, AsyncTaskCompletedHandler handler) {
        return handleReceipt(session, receiptId, handler);
    }

    public static ClaimReceiptTask claimReceipt(Session session, String receiptId) {
        return handleReceipt(session, receiptId, null);
    }

    private static ClaimReceiptTask handleReceipt(Session session, final String receiptId, AsyncTaskCompletedHandler handler) {
        ClaimReceiptTask task;

        synchronized (runningTasks) {
            task = runningTasks.get(receiptId);
            if (task == null) {
                task = new ClaimReceiptTask(session, receiptId);
                task.addAsyncTaskCompletedHandler(new AsyncTaskCompletedHandler<Receipt>() {
                    @Override
                    public void onAsyncTaskFinished() {
                        super.onAsyncTaskFinished();

                        synchronized (runningTasks) {
                            runningTasks.remove(receiptId);
                        }
                    }
                });

                if (handler != null) {
                    task.addAsyncTaskCompletedHandler(handler);
                }
                task.execute();
            }
        }

        return task;
    }

    private ClaimReceiptTask(Session session, String receiptId) {
        super(session);
        mReceiptId = receiptId;
    }

    @Override
    protected Receipt runTask(Void... params) throws Exception {
        ReceiptApi receiptApi = ApiFactory.getInstance(ReceiptApi.class).buildService(getSession());

        ReceiptApi.Receipt receiptApiObject = receiptApi.claimReceipt(mReceiptId, getSession().getUserId());
        Receipt receipt = findOrBuildReceiptFromApiObject(receiptApiObject);

        return receipt;
    }
}

