package com.labv.receipt.tasks;

import android.os.AsyncTask;
import android.util.Log;

import com.labv.receipt.api.ErrorResponse;
import com.labv.receipt.exceptions.LabVApiException;

import java.util.LinkedHashSet;

import retrofit.RetrofitError;

public abstract class HandlerSupportedAsyncTask<P, G, R> extends AsyncTask<P, G, HandlerSupportedAsyncTask.AsyncTaskResponse> {

    public class AsyncTaskResponse<R> {
        Exception exception;
        R result;
    }

    public static abstract class AsyncTaskCompletedHandler<R> {
        public void onAsyncTaskSucceeded(R result) {}
        public void onAsyncTaskFailed(Exception ex) {}
        public void onAsyncTaskCancelled() {}
        public void onAsyncTaskFinished() {}
    }

    protected LinkedHashSet<AsyncTaskCompletedHandler<R>> handlers;

    public void addAsyncTaskCompletedHandler(AsyncTaskCompletedHandler<R> handlerToAdd) {
        if (handlers == null) {
            handlers = new LinkedHashSet<AsyncTaskCompletedHandler<R>>();
        }
        handlers.add(handlerToAdd);
    }

    public void removeAsyncTaskCompletedHandler(AsyncTaskCompletedHandler<R> handler) {
        if (handlers != null) {
            handlers.remove(handler);
        }
    }

    protected abstract R runTask(P... params) throws Exception ;

    @Override
    protected AsyncTaskResponse<R> doInBackground(P... params) {
        AsyncTaskResponse<R> response = new AsyncTaskResponse<R>();
        try {
            response.result = runTask(params);
        } catch (RetrofitError error) {
            if (error.getResponse() != null) {
                ErrorResponse res = (ErrorResponse)error.getBodyAs(ErrorResponse.class);
                LabVApiException apiException = LabVApiException.buildException(res);
                if (apiException != null) {
                    response.exception = apiException;
                }
            }

            if (response.exception == null) {
                response.exception = error;
            }
        } catch (Exception e) {
            Log.e("HSAsyncTask", e.toString());
            response.exception = e;
        }

        return response;
    }

    protected R runAfterInForeground(R r) {
        return r;
    }

    protected R runAfterSuccessInForeground(R r) {
        return r;
    }

    @Override
    protected void onPostExecute(AsyncTaskResponse result) {
        super.onPostExecute(result);
        result.result = runAfterInForeground((R)result.result);
        if (result.exception == null) {
            result.result = runAfterSuccessInForeground((R)result.result);
        }

        if (handlers != null) {
            for (AsyncTaskCompletedHandler<R> handler : handlers) {
                if (result.exception != null) {
                    handler.onAsyncTaskFailed(result.exception);
                } else {
                    handler.onAsyncTaskSucceeded((R) result.result);
                }
                handler.onAsyncTaskFinished();
            }
        }
    }

    @Override
    protected void onCancelled() {
        if (handlers != null) {
            for (AsyncTaskCompletedHandler<R> handler : handlers) {
                handler.onAsyncTaskCancelled();;
                handler.onAsyncTaskFinished();
            }
        }
    }
}
