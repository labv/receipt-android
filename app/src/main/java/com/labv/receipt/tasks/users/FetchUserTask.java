package com.labv.receipt.tasks.users;

import android.util.Log;

import com.labv.receipt.api.ApiFactory;
import com.labv.receipt.api.UserApi;
import com.labv.receipt.models.Session;
import com.labv.receipt.models.User;
import com.labv.receipt.tasks.AuthenticatedHandlerSupportedAsyncTask;

import java.text.SimpleDateFormat;

/**
 * Created by edmundlo on 2015-05-13.
 */
public class FetchUserTask extends AuthenticatedHandlerSupportedAsyncTask<Void, Void, User> {
    private String mUserId;

    private static final Object lock = new Object();
    private static FetchUserTask mTask = null;

    public static FetchUserTask fetchUser(Session session, String userId, AsyncTaskCompletedHandler handler) {
        synchronized (lock) {
            if (mTask == null) {
                mTask = new FetchUserTask(session, userId);
                mTask.addAsyncTaskCompletedHandler(new AsyncTaskCompletedHandler<User>() {
                    @Override
                    public void onAsyncTaskFinished() {
                        super.onAsyncTaskFinished();
                        synchronized (lock) {
                            mTask = null;
                        }
                    }

                    @Override
                    public void onAsyncTaskFailed(Exception ex) {
                        super.onAsyncTaskFailed(ex);
                        Log.e("FetchUserTask", ex.toString());
                    }
                });

                if (handler != null) {
                    mTask.addAsyncTaskCompletedHandler(handler);
                }

                mTask.execute();
            } else {
                Log.i("FetchUserTask", "Fetch in progress, returning existing one");
                handler.onAsyncTaskFinished();
            }
            return mTask;
        }
    }

    private FetchUserTask(Session session, String userId) {
        super(session);
        this.mUserId = userId;
    }

    @Override
    protected User runTask(Void... params) throws Exception {
        UserApi userApi = ApiFactory.getInstance(UserApi.class).buildService(getSession());

        UserApi.User user = userApi.get(mUserId);

        User modelUser = User.find(user.getIdS());
        if (modelUser == null) {
            modelUser = new User();
        }

        modelUser.userId = user.getIdS();
        modelUser.lastName = user.getLastName();
        modelUser.firstName = user.getFirstName();
        modelUser.email = user.getEmail();
        if (user.getDob() != null) {
            modelUser.dateOfBirth = new SimpleDateFormat("yyyy-MM-dd").parse(user.getDob());
        }
        modelUser.save();

        return modelUser;
    }
}
