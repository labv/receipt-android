package com.labv.receipt;

import android.content.Context;
import android.util.Log;

import com.labv.receipt.models.Session;
import com.labv.receipt.tasks.receipts.FetchReceiptsTask;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by edmundlo on 2015-07-21.
 */
public class AutoFetchManager implements PreferenceManager.SessionInfoChangedListener {
    private static int PERIOD = 10000;
    private static AutoFetchManager instance = null;
    private final Context mContext;
    private Session mSession;
    private final Timer mPool;

    public static void initialize(Context context, Session session) {
        if (instance == null) {
            instance = new AutoFetchManager(context, session);
        }
    }

    public static AutoFetchManager getInstance() {
        return instance;
    }

    protected AutoFetchManager(Context context, Session session) {
        this.mContext = context;
        this.mSession = session;
        this.mPool = new Timer();
        watchSessionChanged();
    }

    private void watchSessionChanged() {
        PreferenceManager.getInstance().addSessionListener(new PreferenceManager.SessionInfoChangedListener() {
            @Override
            public void onSessionInfoChanged(Session oldSession, Session newSession) {
                mSession = newSession;
            }
        });
    }

    public void startAutoFetch() {
        this.mPool.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                Log.d("AutoFetchManager", "Start");
                try {
                    if (getSession() == null) {
                        return;
                    }

                    Log.d("AutoFetchManager", "Fetching new receipts");
                    FetchReceiptsTask.fetchNewReceipts(getSession(), null);
                } finally {
                    Log.d("AutoFetchManager", "End");
                }
            }
        }, 0, PERIOD);
    }

    private Session getSession() {
        return this.mSession;
    }

    @Override
    public void onSessionInfoChanged(Session oldSession, Session newSession) {
       this.mSession = newSession;
    }

    public synchronized void submitReceiptForAutoFetch() {
        FetchReceiptsTask.fetchNewReceipts(getSession(), null);
    }
}
