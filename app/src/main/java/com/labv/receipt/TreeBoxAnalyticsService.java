package com.labv.receipt;

import android.os.AsyncTask;
import android.util.Log;

import com.labv.Main;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;

import java.io.IOException;

/**
 * Created by edmundlo on 2015-09-15.
 */
public class TreeBoxAnalyticsService {
    private final String hostname;
    private final String deviceId;
    private OkHttpClient httpClient;
    private static final String TAG = "TreeBoxAnalyticsService";

    public TreeBoxAnalyticsService(String hostname, String deviceId) {
        this.hostname = hostname;
        this.deviceId = deviceId;
        this.httpClient = new OkHttpClient();
    }

    public void fireAppOpenEvent(String userId) {
        String path = String.format(
                "%se=app_visit&source=android&uidh=%s",
                Main.getContext().getString(R.string.analytics_host),
                userId
        );

        fireEvent(path);
    }

    public void firePullToRefreshEvent(String userId) {
        String path = String.format(
                "%se=app_pull&source=android&uidh=%s",
                Main.getContext().getString(R.string.analytics_host),
                userId
        );

        fireEvent(path);
    }

    public void fireReceiptView(String userId, String receiptId) {
        String path = String.format(
                "%se=app_receipt_view&source=android&uidh=%s&r_id=%s",
                Main.getContext().getString(R.string.analytics_host),
                userId,
                receiptId
        );

        fireEvent(path);
    }

    private void fireEvent(final String url) {
        AsyncTask<Void, Void, Void>task = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    Request request = new Request.Builder().url(url).build();
                    httpClient.newCall(request).execute();
                } catch (IOException ex) {
                    Log.e(TAG, ex.toString());
                }
                return null;
            }
        };

        task.execute();
    }
}
