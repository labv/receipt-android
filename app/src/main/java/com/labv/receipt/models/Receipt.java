package com.labv.receipt.models;

import android.content.Context;
import android.provider.BaseColumns;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;
import com.labv.Main;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Table(name = "Receipts", id = BaseColumns._ID)
public class Receipt extends Model {
    @Column(name="ReceiptId", index = true)
    public String receiptId;

    @Column(name="ReceiptClientId")
    public String receiptClientId;

    @Column(name="Retailer")
    public Retailer retailer;

    @Column(name="CreatedDate", index = true)
    public Date createdDate;

    @Column(name="UpdatedDate", index = true)
    public Date updatedDate;

    @Column(name="HtmlPath")
    public String htmlPath;

    @Column(name="Starred")
    public Boolean starred;

    @Column(name="Total")
    public Double total;

    @Column(name="ReadAt")
    public Date readAt;

    public static Receipt latest() {
        return (new Select()).from(Receipt.class).
                orderBy("updatedDate DESC").executeSingle();
    }

    public static Receipt find(String receiptId) {
        return (new Select()).from(Receipt.class).where("receiptId = ?", receiptId).executeSingle();
    }

    public String getFullHtmlPath() {
        if (htmlPath == null) {
            return null;
        }
        return "file://" + Main.getContext().getFilesDir().getPath() + "/" + htmlPath;
    }

    public void saveHtmlString(InputStream htmlString) throws IOException {
        boolean pathChanged = false;
        if (htmlPath == null) {
            htmlPath = UUID.randomUUID().toString();
            pathChanged = true;
        }
        FileOutputStream fos = Main.getContext().openFileOutput(htmlPath, Context.MODE_PRIVATE);
        byte buffer[] = new byte[1024];
        int bytesRead;
        while((bytesRead = htmlString.read(buffer)) != -1) {
            fos.write(buffer, 0, bytesRead);
        }
        fos.close();

        if (pathChanged) {
            this.save();
        }
    }

    public static List<Receipt> findAll(Collection<String> receiptIds) {
        StringBuilder ids = new StringBuilder();
        int i = 0;
        for (String id: receiptIds) {
            if (i != 0) {
                ids.append(",");
            }
            ids.append("'");
            ids.append(id);
            ids.append("'");
            i++;
        }

        return (new Select()).from(Receipt.class).where("receiptId IN ?", ids.toString()).execute();
    }

    public static Integer count() {
        return new Select().from(Receipt.class).execute().size();
    }
}
