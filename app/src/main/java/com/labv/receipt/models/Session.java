package com.labv.receipt.models;

/**
 * Created by edmundlo on 2015-05-13.
 */
public class Session {
    String sessionToken;
    String sessionId;
    String userId;

    public Session(String sessionToken, String sessionId, String userId) {
        this.sessionToken = sessionToken;
        this.sessionId = sessionId;
        this.userId = userId;
    }

    public String getSessionToken() {
        return sessionToken;
    }

    public String getSessionId() {
        return sessionId;
    }

    public String getUserId() {
        return userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Session)) return false;

        Session session = (Session) o;

        if (!sessionId.equals(session.sessionId)) return false;
        if (!sessionToken.equals(session.sessionToken)) return false;
        if (!userId.equals(session.userId)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = sessionToken.hashCode();
        result = 31 * result + sessionId.hashCode();
        result = 31 * result + userId.hashCode();
        return result;
    }
}
