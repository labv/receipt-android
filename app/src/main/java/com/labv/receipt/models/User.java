package com.labv.receipt.models;

import android.provider.BaseColumns;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;
import com.labv.receipt.PreferenceManager;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

@Table(name = "Users", id = BaseColumns._ID)
public class User extends Model {
    @Column(name="FirstName")
    public String firstName;

    @Column(name="LastName")
    public String lastName;

    @Column(name="UserId")
    public String userId;

    @Column(name="Email")
    public String email;

    @Column(name="Dob")
    public Date dateOfBirth;

    public static User find(String userId) {
        return (new Select()).from(User.class).where("userId = ?", userId).executeSingle();
    }

    public static User currentUser() {
        Session info = PreferenceManager.getInstance().getSessionInfo();
        String userId = null;
        if (info != null) {
            userId = info.getUserId();
        }

        if (userId == null) {
            return null;
        }

        return (new Select()).from(User.class).where("userId = ?", userId).executeSingle();
    }

    public String getUserIdHash() {
        byte[] hash = generateHash(userId);
        String hashString = null;
        if (hash != null) {
           hashString = bin2hex(hash);
        }

        return hashString;
    }

    private byte[] generateHash(String input) {
        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("SHA-1");
            digest.reset();
            return digest.digest(input.getBytes());

        } catch (NoSuchAlgorithmException e1) {
            e1.printStackTrace();
        }
        return null;
    }

    private static String bin2hex(byte[] data) {
        return String.format("%0" + (data.length*2) + "X", new BigInteger(1, data));
    }

    public Integer receiptCount() {
        return Receipt.count();
    }
}
