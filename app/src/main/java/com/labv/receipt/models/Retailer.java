package com.labv.receipt.models;

import android.provider.BaseColumns;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.Date;
import java.util.List;

@Table(name = "Retailers", id = BaseColumns._ID)
public class Retailer extends Model {
    @Column(name="LogoImageUrl")
    public String logoImageUrl;

    @Column(name="Name")
    public String name;

    @Column(name="RetailerId")
    public String retailerId;

    @Column(name="LastFetchAt")
    public Date lastFetchedAt;

    @Column(name="Category")
    public String category;

    // This method is optional, does not affect the foreign key creation.
    public List<Receipt> receipts() {
        return getMany(Receipt.class, "Retailer");
    }

    public String getLogoImageUrl() {
        return logoImageUrl;
    }

    public String getName() {
        return name;
    }

    public String getRetailerId() {
        return retailerId;
    }

    public Date getLastFetchedAt() {
        return lastFetchedAt;
    }

    public static List<Retailer> all() {
        return (new Select()).from(Retailer.class).execute();
    }

    public static synchronized Retailer findOrCreate(String retailerId) {
        Retailer retailer = (new Select()).from(Retailer.class).where("retailerId = ?", retailerId).executeSingle();
        if (retailer == null) {
            retailer = new Retailer();
            retailer.retailerId = retailerId;
            retailer.save();
        }
        return retailer;
    }

    public static Retailer find(int id) {
        return (new Select()).from(Retailer.class).where("_id = ?", id).executeSingle();
    }
}
