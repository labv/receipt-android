package com.labv.receipt.activities;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.labv.Main;
import com.labv.receipt.R;
import com.labv.receipt.fragments.LabvBaseFragment;
import com.labv.receipt.fragments.LeftMenuFragment;
import com.labv.receipt.fragments.ReceiptDetailsFragment;
import com.labv.receipt.fragments.ReceiptStreamFragment;
import com.labv.receipt.fragments.UpdatePasswordFragment;
import com.labv.receipt.fragments.UserProfileFragment;
import com.labv.receipt.services.gcm.RegistrationIntentService;
import com.labv.receipt.tasks.HandlerSupportedAsyncTask;

public class MainActivity extends AuthenticatedActivityBase implements LeftMenuFragment.OnLeftMenuItemClicked, ReceiptStreamFragment.ReceiptStreamItemClickedListener {

    private DrawerLayout mDrawerLayout;
    private static int LEFT_DRAWER_ID = R.id.main_left_drawer;
    public static int MAIN_CONTENT_ID = R.id.main_content_frame;
    private ActionBarDrawerToggle mDrawerToggle;
    private String currentView = LeftMenuFragment.MY_RECEIPTS;
    private ReceiptStreamFragment allReceiptsView = null;
    private ReceiptStreamFragment starredReceiptsView = null;
    private LabvBaseFragment currentFragment = null;

    @Override
    protected String getScreenTitle() {
        return "MainActivity";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        updateFromServer(null);
        setContentView(R.layout.activity_main);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.main_drawer_layout);

        if (savedInstanceState == null) {
            currentFragment = setupMainFrame(false);
        }

        setupDrawerToggle();

        if (checkPlayServices()) {
            // Start IntentService to register this application with GCM.
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }

    }

    private String buildNotificationMessage() {
        if (!Main.isNetworkAvailable()) {
            return getString(R.string.network_not_available);
        } else if (!getTransceiverManager().nfcAvailable()) {
            return getString(R.string.nfc_not_available);
        } else if (!getTransceiverManager().nfcEnabled()) {
            return getString(R.string.nfc_not_enabled);
        } else {
            return null;
        }
    }

    private void updateNotificationMessage(ReceiptStreamFragment stream) {
        stream.setNotificationMessage(buildNotificationMessage());
    }

    protected LabvBaseFragment setupMainFrame(boolean starred) {
        ReceiptStreamFragment fragment = starred ? starredReceiptsView : allReceiptsView;

        if (fragment == null) {
            fragment = ReceiptStreamFragment.newInstance(starred);
        }

        getActionBar().setTitle(fragment.getTitle());
        getFragmentManager().beginTransaction()
                .replace(MAIN_CONTENT_ID, fragment)
                .commit();


        if (starred) {
            starredReceiptsView = fragment;
        } else {
            allReceiptsView = fragment;
        }

        updateNotificationMessage(fragment);

        return fragment;
    }

    protected void pushUserProfileFragement() {
        UserProfileFragment fragment = UserProfileFragment.newInstance(getUser());
        getActionBar().setTitle(fragment.getTitle());
        currentFragment = fragment;
        getFragmentManager().beginTransaction().
                replace(MAIN_CONTENT_ID, fragment).commit();
    }

    protected void pushChangePasswordFragement() {
        UpdatePasswordFragment fragment = UpdatePasswordFragment.newInstance();
        getActionBar().setTitle(fragment.getTitle());
        currentFragment = fragment;
        getFragmentManager().beginTransaction().
                replace(MAIN_CONTENT_ID, fragment).commit();
    }

    protected void pushReceiptDetailFragment(String receiptId) {
        LabvBaseFragment fragment = ReceiptDetailsFragment.newInstance(receiptId);
        getActionBar().setTitle(fragment.getTitle());
        currentFragment = fragment;
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        getFragmentManager().beginTransaction().
                add(MAIN_CONTENT_ID, fragment).addToBackStack(null).
                setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN).commit();
    }

    @Override
    public void onBackPressed() {
        if (!tryPopFragment()) {
            Log.i("MainActivity", "nothing on backstack, calling super");
            super.onBackPressed();
        }
    }

    public boolean tryPopFragment() {
        FragmentManager fm = getFragmentManager();
        if (fm.getBackStackEntryCount() > 0) {
            Log.i("MainActivity", "popping backstack");
            fm.popBackStackImmediate();

            if (fm.getBackStackEntryCount() <= 0) {
                mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            } else {
                mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            }

            LabvBaseFragment f = (LabvBaseFragment) getFragmentManager().findFragmentById(R.id.main_content_frame);
            currentFragment = f;
            getActionBar().setTitle(f.getTitle());

            return true;
        } else {
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            return false;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (currentFragment instanceof ReceiptStreamFragment) {
            updateNotificationMessage((ReceiptStreamFragment) currentFragment);
        }

        ((Main)getApplication()).getAnalyticsService().fireAppOpenEvent(getUser().getUserIdHash());
    }

    /* Called whenever we call invalidateOptionsMenu() */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // If the nav drawer is open, hide action items related to the content view
        return super.onPrepareOptionsMenu(menu);
    }

    private void setupDrawerToggle() {
//        getActionBar().setHomeButtonEnabled(true);
//        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {
//            @Override
//            public void onDrawerClosed(View view) {
//                super.onDrawerClosed(view);
//                MainActivity.this.onDrawerClosed(view);
//                getActionBar().setTitle(currentFragment.getTitle());
//            }
//
//            @Override
//            public void onDrawerOpened(View drawerView) {
//                super.onDrawerOpened(drawerView);
//                MainActivity.this.onDrawerOpened(drawerView);
//            }
//
//            @Override
//            public void onConfigurationChanged(Configuration newConfig) {
//                super.onConfigurationChanged(newConfig);
//                mDrawerToggle.syncState();
//            }
//        };

        // Set the drawer toggle as the DrawerListener
//        mDrawerLayout.setDrawerListener(mDrawerToggle);
//        mDrawerToggle.syncState();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return super.onOptionsItemSelected(item);
    };

    @Override
    public void updateFromServer(HandlerSupportedAsyncTask.AsyncTaskCompletedHandler handler) {
        super.updateFromServer(handler);
    }

    @Override
    public void onItemClicked(final String item) {
        mDrawerLayout.closeDrawers();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (currentView != item) {
                    switch (item) {
                        case LeftMenuFragment.MY_RECEIPTS:
                            currentFragment = setupMainFrame(false);
                            break;
                        case LeftMenuFragment.STARRED:
                            currentFragment = setupMainFrame(true);
                            break;
                        case LeftMenuFragment.MY_PROFILE:
                            pushUserProfileFragement();
                            break;
                        case LeftMenuFragment.CHANGE_PASSWORD:
                            pushChangePasswordFragement();
                            break;
                        case LeftMenuFragment.LOG_OUT:
                            logout();
                            break;
                    }
                    currentView = item;
                }
            }
        }, 200);
    }

    @Override
    public void onReceiptClicked(String receiptId) {
        pushReceiptDetailFragment(receiptId);
    }
}
