package com.labv.receipt.activities;

import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.NfcEvent;
import android.nfc.Tag;
import android.nfc.tech.IsoDep;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.Toast;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.Configuration;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.labv.Main;
import com.labv.nfc.TransceiverManager;
import com.labv.receipt.AutoFetchManager;
import com.labv.receipt.PreferenceManager;
import com.labv.receipt.models.Session;
import com.labv.receipt.models.User;
import com.labv.receipt.tasks.HandlerSupportedAsyncTask;
import com.labv.receipt.tasks.receipts.ClaimReceiptTask;
import com.labv.receipt.tasks.receipts.FetchReceiptsTask;
import com.labv.receipt.tasks.retailers.FetchRetailerTask;
import com.labv.receipt.tasks.sessions.LogoutTask;
import com.labv.receipt.tasks.users.FetchUserTask;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by edmundlo on 2015-05-19.
 */
public abstract class AuthenticatedActivityBase extends FragmentActivity
implements PreferenceManager.SessionInfoChangedListener, NfcAdapter.OnNdefPushCompleteCallback, NfcAdapter.CreateNdefMessageCallback{

    private static String mDatabaseName = null;
    private static Session mSession = null;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String TAG = "AuthActivityBase";

    private PendingIntent pendingIntent;
    private IntentFilter[] mFilters;
    private NfcAdapter mAdapter;
    private TransceiverManager mTransceiverManager;

    private static Pattern ReceiptUrlPattern = Pattern.compile(".*gettreebox.com\\/welcome\\/receipts");
    private static String DefaultProtocol = "http";

    protected abstract String getScreenTitle();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupNfc();

        ((Main)getApplication()).getDefaultTracker().setScreenName(getScreenTitle());
        ((Main)getApplication()).getDefaultTracker().send(new HitBuilders.ScreenViewBuilder().build());

        mSession = PreferenceManager.getInstance().getSessionInfo();
        if (mSession == null) {
            launchWelcomeActivity();
        }

        String databaseName = getDatabaseName();

        if (databaseName != mDatabaseName) {
            Configuration.Builder builder = new Configuration.Builder(this);
            builder.setDatabaseName(databaseName);
            ActiveAndroid.dispose();
            ActiveAndroid.initialize(builder.create());
            mDatabaseName = databaseName;
        }
    }

    protected void trackScreenView(String name) {
        ((Main)getApplication()).getDefaultTracker().setScreenName(name);
        ((Main)getApplication()).getDefaultTracker().send(new HitBuilders.ScreenViewBuilder().build());
   }

    public TransceiverManager getTransceiverManager() {
       return mTransceiverManager;
    }

    protected void setupNfc() {
        mTransceiverManager = new TransceiverManager(getApplicationContext());
        if (!mTransceiverManager.nfcAvailable()) {
            return;
        }
        mAdapter = NfcAdapter.getDefaultAdapter(this);
        pendingIntent = PendingIntent.getActivity(
                this,
                0,
                new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP),
                0
        );
        IntentFilter td = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
        mFilters = new IntentFilter[] {
                td
        };
    }

    protected void enableNfc() {
        if (!mTransceiverManager.nfcAvailable()) {
            return;
        }
        mAdapter.enableForegroundDispatch(this, pendingIntent, mFilters, new String[0][0]);

        mAdapter.setOnNdefPushCompleteCallback(this, this);
        mAdapter.setNdefPushMessageCallback(this, this);
    }

    public NdefMessage createNdefMessage(NfcEvent event) {
        NdefRecord uriRecord = NdefRecord.createUri("http://gettreebox.com/welcome/receipts?id=55f390dd646576148dbc0800");
        NdefMessage message = new NdefMessage(new NdefRecord[] { uriRecord });
        return message;
    }

    @Override
    public void onNdefPushComplete(NfcEvent event) {
        int i = 0;
    }

    protected void disableNfc() {
        if (!mTransceiverManager.nfcAvailable()) {
            return;
        }
        mAdapter.disableForegroundDispatch(this);
    }

    public User getUser() {
        return User.find(getSession().getUserId());
    }

    protected void updateFromServer(final HandlerSupportedAsyncTask.AsyncTaskCompletedHandler handler) {
        if (Main.isNetworkAvailable()) {
            final AtomicInteger workLeft = new AtomicInteger(2);
            HandlerSupportedAsyncTask.AsyncTaskCompletedHandler overallHandler = new HandlerSupportedAsyncTask.AsyncTaskCompletedHandler() {
                @Override
                public void onAsyncTaskFinished() {
                    super.onAsyncTaskFinished();
                    if (workLeft.decrementAndGet() <= 0) {
                        if (handler != null) {
                            handler.onAsyncTaskFinished();
                        }
                    }
                }
            };
            FetchUserTask.fetchUser(mSession, mSession.getUserId(), overallHandler);
            FetchReceiptsTask.fetchNewReceipts(mSession, overallHandler);
            FetchRetailerTask.updateRetailers(mSession);
        } else {
           if (handler != null)  {
               handler.onAsyncTaskFinished();
           }
        }
    }

    private String getDatabaseName() {
        Session session = PreferenceManager.getInstance().getSessionInfo();
        return String.format("db-%s", session.getUserId());
    }

    public Session getSession() {
        return mSession;
    }

    @Override
    protected void onResume() {
        super.onResume();
        PreferenceManager.getInstance().addSessionListener(this);
        if (PreferenceManager.getInstance().getSessionInfo() == null) {
            launchWelcomeActivity();
        }

        enableNfc();

        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(getIntent().getAction())) {
            Tag t = getIntent().getParcelableExtra(NfcAdapter.EXTRA_TAG);
            onNfcTagDetected(t);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        disableNfc();
        PreferenceManager.getInstance().removeSessionListener(this);
    }

    @Override
    public void onSessionInfoChanged(Session oldSession, Session newSession) {
        if (newSession == null) {
            launchWelcomeActivity();
        }
    }

    protected void launchWelcomeActivity() {
        Intent newIntent = new Intent(this, WelcomeActivity.class);
        newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(newIntent);
    }

    protected void logout() {
        (new LogoutTask(PreferenceManager.getInstance().getSessionInfo())).execute();
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    protected boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    @Override
    public void onNewIntent(Intent intent){
        // fetch the tag from the intent
        Tag tag = null;
        Collection<NdefMessage> ndefMessages = new ArrayList<NdefMessage>();

        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())) {
            tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            Parcelable[] rawMsgs = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
            if (rawMsgs != null) {
                for (int i = 0; i < rawMsgs.length; i++) {
                    ndefMessages.add((NdefMessage) rawMsgs[i]);
                }
            }
        }

        handleNfcMessages(tag, ndefMessages);
    }

    private void handleNfcMessages(Tag tag, Collection<NdefMessage> ndefMessages) {
        String receiptId = null;

        if (ndefMessages != null)  {
            receiptId = extractReceiptIdFromNdefMessages(ndefMessages);
        }

        if (receiptId != null) {
            ClaimReceiptTask.claimReceipt(getSession(), receiptId);
        } else if (tag != null) {
            onNfcTagDetected(tag);
        }
    }

    private String extractReceiptIdFromNdefMessages(Collection<NdefMessage> ndefMessages) {
        for(NdefMessage ndefMessage : ndefMessages) {
            for (NdefRecord record : ndefMessage.getRecords()) {
                String message = extractNdefMessage(record);
                Matcher matcher = ReceiptUrlPattern.matcher(message);

                if (matcher.find()) {
                    try {
                        String receiptId = null;
                        message = message.replace("\u0003", String.format("%s://", DefaultProtocol));
                        URL url = new URL(message);
                        Map<String, String> query = extractQueryData(url);
                        receiptId = query.get("id");

                        if (receiptId != null) {
                            return receiptId;
                        }
                    } catch (MalformedURLException ex) {
                        Log.e(TAG, ex.toString());
                    } catch (UnsupportedEncodingException ex) {
                        Log.e(TAG, ex.toString());
                    }
                }
            }
        }

        return null;
    }

    private Map<String, String> extractQueryData(URL url) throws UnsupportedEncodingException {
        Map<String, String> query_pairs = new LinkedHashMap<String, String>();
        String query = url.getQuery();
        String[] pairs = query.split("&");
        for (String pair : pairs) {
            int idx = pair.indexOf("=");
            query_pairs.put(URLDecoder.decode(pair.substring(0, idx), "UTF-8"), URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
        }
        return query_pairs;
    }

    private String extractNdefMessage(NdefRecord record) {
        String payload = null;

        byte[] recordContent = record.getPayload();

        try {
            payload = new String(recordContent, "UTF-8");
        } catch (Exception e) {
           Log.e(TAG, e.toString());
        }

        return payload;
    }

    private void onNfcTagDetected(Tag tag) {
        String status = "Tag detected. ";

        if (!mTransceiverManager.readyToTransceiver()) {
            status += "Transceiver manager busy." ;
            Log.i("AuthBase", status);

        }

        String userId = PreferenceManager.getInstance().getSessionInfo().getUserId();
        IsoDep apduTag = IsoDep.get(tag);
        if (apduTag == null) {
            status += "Tag is not IsoDep";
            Log.i("AuthBase", status);
            return;
        }

        Log.i("AuthBase", "Transceiving.... Content: 0x00 0x14 0x00 0x80 " + userId);

        mTransceiverManager.submitTransceive(
                apduTag,
                userId.getBytes(Charset.forName("US-ASCII")),
                new TransceiverManager.TransceiveCompletedListener() {
                    @Override
                    public void onTransceiveCompleted(byte[] result) {
                        AuthenticatedActivityBase.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(AuthenticatedActivityBase.this, "Transceive completed",Toast.LENGTH_LONG).show();
                                AutoFetchManager.getInstance().submitReceiptForAutoFetch();
                            }
                        });
                    }

                    @Override
                    public void onTransceiveFailed(final Exception ex) {
                        AuthenticatedActivityBase.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(AuthenticatedActivityBase.this, "Transceive failed " + ex.toString(), Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                }
        );
    }
}

