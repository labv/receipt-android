package com.labv.receipt.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.labv.receipt.R;
import com.labv.receipt.tasks.HandlerSupportedAsyncTask;
import com.labv.receipt.tasks.sessions.ResetPasswordTask;

public class ForgotPasswordActivity extends UnauthenticatedActivityBase {

    private Button mResetPasswordEmailButton;
    private EditText mEmailView;
    private View mResetPasswordFormView;
    private ProgressBar mProgressView;
    private ResetPasswordTask mAuthTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        mResetPasswordEmailButton = (Button) findViewById(R.id.reset_password_button);
        mResetPasswordEmailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetPassword();
            }
        });
        mEmailView = (EditText) findViewById(R.id.email);
        mResetPasswordFormView = (View) findViewById(R.id.email_forgot_password_form);
        mProgressView = (ProgressBar) findViewById(R.id.forgot_password_progress);
    }

    @Override
    protected String getScreenTitle() {
        return getString(R.string.forgot_password);
    }

    private void resetPassword() {

        // Reset errors.
        mEmailView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();

        boolean cancel = false;
        View focusView = null;


        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            mAuthTask = new ResetPasswordTask(email);
            mAuthTask.addAsyncTaskCompletedHandler(new HandlerSupportedAsyncTask.AsyncTaskCompletedHandler() {
                @Override
                public void onAsyncTaskSucceeded(Object result) {
                    super.onAsyncTaskSucceeded(result);
                    Toast.makeText(
                            ForgotPasswordActivity.this.getApplicationContext(),
                            getString(R.string.reset_password_success), Toast.LENGTH_LONG).show();
                    finish();
                }

                @Override
                public void onAsyncTaskFailed(Exception ex) {
                    super.onAsyncTaskFailed(ex);

                    Toast.makeText(ForgotPasswordActivity.this.getApplicationContext(), "Failed to reset password:", Toast.LENGTH_LONG).show();
                }

                @Override
                public void onAsyncTaskCancelled() {
                    super.onAsyncTaskCancelled();
                }

                @Override
                public void onAsyncTaskFinished() {
                    super.onAsyncTaskFinished();
                    mAuthTask = null;
                    showProgress(false);
                }
            });
            mAuthTask.execute();
        }
    }


    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mResetPasswordFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mResetPasswordFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mResetPasswordFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mResetPasswordFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }
}
