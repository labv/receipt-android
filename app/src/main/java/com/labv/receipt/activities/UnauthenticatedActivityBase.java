package com.labv.receipt.activities;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.gms.analytics.HitBuilders;
import com.labv.Main;
import com.labv.receipt.PreferenceManager;

/**
 * Created by edmundlo on 2015-05-19.
 */
public abstract class UnauthenticatedActivityBase extends ActivityBase {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (PreferenceManager.getInstance().getSessionInfo() != null) {
            launchMainActivity();
        }
    }

    protected abstract String getScreenTitle();

    @Override
    protected void onResume() {
        super.onResume();
        ((Main)getApplication()).getDefaultTracker().setScreenName(getScreenTitle());
        ((Main)getApplication()).getDefaultTracker().send(new HitBuilders.ScreenViewBuilder().build());
    }

    protected void launchMainActivity() {
        Intent newIntent = new Intent(this, MainActivity.class);
        newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(newIntent);
    }

    protected boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    protected boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }


}
