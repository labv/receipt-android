package com.labv.receipt.activities;

import android.app.Activity;
import android.os.Bundle;

public abstract class ActivityBase extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
