package com.labv.receipt.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.labv.receipt.R;

public class WelcomeActivity extends UnauthenticatedActivityBase {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        registerButtonCallbacks();
    }

    @Override
    protected String getScreenTitle() {
        return "WelcomeActivity";
    }

    private void registerButtonCallbacks() {
        findViewById(R.id.welcome_btn_create_account).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(WelcomeActivity.this, CreateUserActivity.class);
                WelcomeActivity.this.startActivity(myIntent);
            }
        });

        findViewById(R.id.welcome_btn_login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(WelcomeActivity.this, LoginActivity.class);
                WelcomeActivity.this.startActivity(myIntent);
            }
        });
    }
}
