package com.labv.receipt.adapters;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.koushikdutta.ion.Ion;
import com.labv.receipt.R;
import com.labv.receipt.models.Retailer;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by edmundlo on 2015-06-05.
 */
public class MainStreamCursorAdapter extends CursorAdapter {

    public interface OnStarClickedListener {
        public void onStarClicked(String receiptId, ImageView view);
    }

    private final Map<Integer, Retailer> mRetailerCache = new HashMap<>();
    private final DateFormat mDisplayFormat = new SimpleDateFormat("dd-MM-yyyy");

    private OnStarClickedListener mListener;

    public MainStreamCursorAdapter(Context context, Cursor c, boolean autoRequery, OnStarClickedListener listener) {
        super(context, c, autoRequery);
        mListener = listener;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        return inflater.inflate(R.layout.main_stream_row, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        final String receiptId = cursor.getString(cursor.getColumnIndex("ReceiptId"));
        ImageView iconView = (ImageView) view.findViewById(R.id.icon);
        final ImageView starView = (ImageView) view.findViewById(R.id.favouriteButton);
        view.findViewById(R.id.favouriteClickArea).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onStarClicked(receiptId, starView);
            }
        });

        if (cursor.getInt(cursor.getColumnIndex("Starred")) > 0) {
            starView.setImageResource(R.drawable.star);
        } else {
            starView.setImageResource(R.drawable.empty_star);
        }

        TextView retailerNameView = (TextView) view.findViewById(R.id.firstLine);
        TextView dateView = (TextView) view.findViewById(R.id.dateLine);
        TextView totalView = (TextView) view.findViewById(R.id.totalLine);

        boolean read = !cursor.isNull(cursor.getColumnIndex("ReadAt"));

        if (read) {
            retailerNameView.setTypeface(null, Typeface.NORMAL);
            dateView.setTypeface(null, Typeface.NORMAL);
            totalView.setTypeface(null, Typeface.NORMAL);
        } else {
            retailerNameView.setTypeface(null, Typeface.BOLD);
            dateView.setTypeface(null, Typeface.BOLD);
            totalView.setTypeface(null, Typeface.BOLD);
        }

        int retailerId = cursor.getInt(cursor.getColumnIndex("Retailer"));
        Retailer retailer = mRetailerCache.get(retailerId);
        String dateString = cursor.getString(cursor.getColumnIndex("CreatedDate"));
        Double total = null;
        if (!cursor.isNull(cursor.getColumnIndex("Total"))) {
            total  = cursor.getDouble(cursor.getColumnIndex("Total"));
        }

        Date date = null;

        if (dateString != null) {
            date = new Date(Long.parseLong(dateString));
        }

        if (retailer == null) {
            retailer = Retailer.find(retailerId);
            mRetailerCache.put(retailerId, retailer);
        }

        if (retailer != null) {
            if (retailer.logoImageUrl != null) {
                Ion.with(iconView).fitCenter().load(retailer.logoImageUrl);
            }

            if (retailer.name != null) {
                retailerNameView.setText(retailer.name);
            }
        }

        if (date != null) {
            String displayDate = String.format("Date: %s", mDisplayFormat.format(date));
            dateView.setText(displayDate);
        }

        if (total != null) {
            totalView.setText(String.format(context.getString(R.string.stream_total), total));
            totalView.setVisibility(View.VISIBLE);
        } else {
            totalView.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public Cursor swapCursor(Cursor newCursor) {
        mRetailerCache.clear();
        return super.swapCursor(newCursor);
    }
}
