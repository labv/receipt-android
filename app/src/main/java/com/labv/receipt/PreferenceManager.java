package com.labv.receipt;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Looper;

import com.labv.receipt.models.Session;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by edmundlo on 2015-05-13.
 */
public class PreferenceManager {

    public interface SessionInfoChangedListener {
       public void onSessionInfoChanged(Session oldSession, Session newSession);
    }

    private static PreferenceManager instance = null;
    private final Context mContext;
    private static final String PUSH_TOKEN = "PUSH_TOKEN";
    private static final String USER_ID = "userId";
    private static final String SESSION_ID = "sessionId";
    private static final String SESSION_TOKEN = "sessionToken";

    private final Set<SessionInfoChangedListener> mSessionListeners;

    public static void initialize(Context context) {
        instance = new PreferenceManager(context);
    }

    protected PreferenceManager(Context context) {
        this.mContext = context;
        this.mSessionListeners = new LinkedHashSet<SessionInfoChangedListener>();
    }

    private SharedPreferences.Editor getEditor() {
        return getPref().edit();
    }

    private SharedPreferences getPref() {
        return mContext.getSharedPreferences("ReceiptPref", 0); // 0 - for private mode
    }

    public static PreferenceManager getInstance() {
        return instance;
    }

    private void notifySessionListeners(final Session oldSession, final Session newSession) {
        if (Looper.getMainLooper() != Looper.myLooper()) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    for (SessionInfoChangedListener listener : mSessionListeners) {
                        listener.onSessionInfoChanged(oldSession , newSession);
                    }
                }
            });
        } else {
            for (SessionInfoChangedListener listener : mSessionListeners) {
                listener.onSessionInfoChanged(oldSession , newSession);
            }
        }
    }

    public void addSessionListener(SessionInfoChangedListener listener) {
        mSessionListeners.add(listener);
    }

    public void removeSessionListener(SessionInfoChangedListener listener) {
        mSessionListeners.remove(listener);
    }

    public void removeSessionInfo(Session sessionInfo) {
        if (sessionInfo == null) {
            return;
        }
        SharedPreferences.Editor editor = getEditor();

        synchronized (mSessionListeners) {
            Session oldSession = getSessionInfo();
            if (sessionInfo.equals(oldSession)) {
                editor.putString(USER_ID, null);
                editor.putString(SESSION_ID, null);
                editor.putString(SESSION_TOKEN, null);
            }

            editor.commit();
            notifySessionListeners(sessionInfo, null);
        }
    }

    public void setSessionInfo(Session sessionInfo) {
        if (sessionInfo == null ||
                sessionInfo.getSessionId() == null ||
                sessionInfo.getSessionToken() == null ||
                sessionInfo.getUserId() == null) {
            throw new IllegalArgumentException("Session cannot be null");
        }
        SharedPreferences.Editor editor = getEditor();

        Session oldSession = getSessionInfo();
        synchronized (mSessionListeners) {
            editor.putString(USER_ID, sessionInfo.getUserId());
            editor.putString(SESSION_ID, sessionInfo.getSessionId());
            editor.putString(SESSION_TOKEN, sessionInfo.getSessionToken());
            editor.commit();
            notifySessionListeners(oldSession, sessionInfo);
        }
    }

    public Session getSessionInfo() {
        SharedPreferences pref = getPref();

        String sessionToken = pref.getString(SESSION_TOKEN, null);
        String sessionId = pref.getString(SESSION_ID, null);
        String userId = pref.getString(USER_ID, null);

        if (sessionId == null || sessionToken == null || userId == null) {
           return null;
        }

        return new Session(sessionToken, sessionId, userId);
    }

    public void setPushToken(String token) {
        getPref().edit().putString(PUSH_TOKEN, token).apply();
    }


    public String getPushToken() {
        return getPref().getString(PUSH_TOKEN, null);
    }
}
