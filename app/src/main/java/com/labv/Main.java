package com.labv;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.labv.receipt.AutoFetchManager;
import com.labv.receipt.PreferenceManager;
import com.labv.receipt.R;
import com.labv.receipt.TreeBoxAnalyticsService;
import com.labv.receipt.api.ApiFactory;

public class Main extends Application {
    private static Context context = null;
    public static final String ENV = "dev";
    private Tracker mTracker;
    private TreeBoxAnalyticsService analyticsService = null;

    @Override
    public void onCreate() {
        super.onCreate();
        context = this.getApplicationContext();
        ApiFactory.initialize(ENV);
        PreferenceManager.initialize(getApplicationContext());
        AutoFetchManager.initialize(getApplicationContext(), PreferenceManager.getInstance().getSessionInfo());
//        AutoFetchManager.getInstance().startAutoFetch();
        analyticsService = new TreeBoxAnalyticsService(getString(R.string.analytics_host), getDeviceId());
    }

    /**
     * Gets the default {@link Tracker} for this {@link Application}.
     * @return tracker
     */
    synchronized public Tracker getDefaultTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            mTracker = analytics.newTracker(getString(R.string.ga_tracker_id));
            mTracker.enableExceptionReporting(true);
            mTracker.enableAdvertisingIdCollection(true);
            mTracker.enableAutoActivityTracking(true);
        }
        return mTracker;
    }

    public TreeBoxAnalyticsService getAnalyticsService() { return this.analyticsService; }

    public static Context getContext() {
        return context;
    }

    public static String getDeviceId() {
        if (context == null) {
            throw new IllegalStateException("Context not set");
        }

        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
