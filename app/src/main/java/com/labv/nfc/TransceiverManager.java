package com.labv.nfc;

import android.content.Context;
import android.content.pm.PackageManager;
import android.nfc.NfcAdapter;
import android.nfc.NfcManager;
import android.nfc.tech.IsoDep;
import android.util.Log;

import com.labv.utils.StringUtils;

import java.io.IOException;

/**
 * Created by edmundlo on 2015-05-06.
 */
public class TransceiverManager {

    public interface TransceiveCompletedListener {
        public void onTransceiveCompleted(final byte[] result);
        public void onTransceiveFailed(final Exception ex);
    }

    private String mLogName = "TransceiverManager";
    private Thread mTransceiverThread;
    private Context mContext;
    private Object mThreadLock;

    public TransceiverManager(Context context) {
        mTransceiverThread = null;
        mThreadLock = new Object();
        this.mContext = context;
    }

    public boolean nfcAvailable() {
        return mContext.getPackageManager().hasSystemFeature(PackageManager.FEATURE_NFC);
    }

    public boolean nfcEnabled() {
        NfcManager manager = (NfcManager) mContext.getSystemService(Context.NFC_SERVICE);
        NfcAdapter adapter = manager.getDefaultAdapter();
        return adapter != null && adapter.isEnabled();
    }

    public boolean readyToTransceiver() {
        synchronized(mThreadLock) {
            return mTransceiverThread == null || !mTransceiverThread.isAlive();
        }
    }

    private static byte[] HEADERS = {
            (byte) 0x00, // CLA Class
            (byte) 0x14, //
            (byte) 0x00, // P1  Parameter 1
            (byte) 0x00, // P2  Parameter 2
    };

    public boolean submitTransceive(IsoDep tag, final byte[] userId, final TransceiveCompletedListener listener) {
        final IsoDep adapter = tag;
        final byte[] content = new byte[HEADERS.length + 1 + userId.length];

        System.arraycopy(HEADERS, 0, content, 0, HEADERS.length);
        content[HEADERS.length] = (byte)userId.length;
        System.arraycopy(userId, 0, content, HEADERS.length + 1, userId.length);

        Log.e("NfcDemo", StringUtils.toHexString(content));

        synchronized (mThreadLock) {
            if (!readyToTransceiver()) {
                return false;
            }

            this.mTransceiverThread = new Thread() {
                public void run() {
                    try {
                        Log.e("NfcDemo", "Attempting connection");

                        adapter.connect();

                        if (adapter.isConnected()) {
                            Log.e("NfcDemo", "Connected");
                            Log.e("NfcDemo", "Transceiving");

                            byte[] result =  new byte[0];
                            boolean success = false;

                            for (int i = 0 ; i < 10 ; i ++) {
                                try {
                                    Log.e("NfcDemo", "Sending: " + StringUtils.toHexString(content));
                                    result = adapter.transceive(content);
                                    Log.e("NfcDemo", "Result: " + StringUtils.toHexString(result));
                                    Log.e("NfcDemo", "Trial: " + i);

                                    success = true;
                                } catch (Exception e) {
                                    Log.e("NfcDemo", "Error " + e.toString());
                                    success = false;
                                }

                                if (success) {
                                    break;
                                }
                            }

                            listener.onTransceiveCompleted(result);
                        } else {
                            Log.e("NfcDemo", "Not connected");
                        }
                    } catch (IOException ex) {
                        listener.onTransceiveFailed(ex);
                    } finally {
                        try {
                            adapter.close();
                        } catch (IOException ex) {}
                    }
                }
            };

            this.mTransceiverThread.start();

            return true;
        }
    }
}
