package com.labv.utils;

/**
 * Created by edmundlo on 2015-05-24.
 */
public class StringUtils {
    public static String toHexString(byte[] bytes) {
        StringBuilder builder = new StringBuilder();
        for (byte b : bytes) {
            builder.append(String.format("\\x%02x", b));
        }

        return builder.toString();
    }
}
